<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(
    [
        'prefix' => LaravelLocalization::setLocale(),
        'middleware' => ['localeSessionRedirect', 'localizationRedirect', 'localeViewPath']
    ],
    function () {

        Route::get('/', 'MainController@index');

        Route::get('/blogs', 'MainController@blogs');
        Route::get('/blogs/{alias}', 'MainController@blog');
        Route::get('/contacts', 'MainController@contacts');
        Route::get('/portfolio', 'MainController@portfolioList');
        Route::get('/actions', 'MainController@actions');
//        Route::get('/agreement', 'MainController@agreement');
//        Route::get('/policy', 'MainController@policy');
        Route::get('/jobs', 'MainController@jobs');
        Route::get('/partners', 'MainController@partners');
        Route::get('/thanks', 'MainController@thanks');
        Route::get('/pages/{alias}', 'MainController@pages');
        Route::get('/download', function(){
            return response()->download(config("pdf_file"));
        });
        Route::get('sitemap', function() {
            $sitemap = App::make('sitemap');
            $sitemap->setCache('laravel.sitemap', 60);
            if (!$sitemap->isCached()) {
                $sitemap->add(URL::to('/'), '2019-01-23:10:00+02:00', '1.0', 'daily');
                $sitemap->add(URL::to('/portfolio'), '2019-01-23T12:30:00+02:00', '0.9', 'monthly');
                $sitemap->add(URL::to('/blogs'), '2019-01-23T12:30:00+02:00',  '1.0', 'daily');
                $sitemap->add(URL::to('/actions'), '2019-01-23T12:30:00+02:00',  '1.0', 'daily');
                $sitemap->add(URL::to('/agreement'), '2019-01-23T12:30:00+02:00', '0.9', 'monthly');
                $sitemap->add(URL::to('/policy'), '2019-01-23T12:30:00+02:00', '0.9', 'monthly');
                $sitemap->add(URL::to('/jobs'), '2019-01-23T12:30:00+02:00', '0.9', 'monthly');
                $sitemap->add(URL::to('/partners'), '2019-01-23T12:30:00+02:00', '0.9', 'monthly');
                $portfolios = \App\Models\PortfolioModel::where('status', \App\Models\ChooseModel::STATUS_SHOW)->orderBy('position', 'asc')->get();
                foreach ($portfolios as $portfolio) {
                    $images = [];
                    if(!empty($portfolio->sliders)){
                        foreach($portfolio->sliders as $slider){
                            $images[] = ['url' => URL::to("/storage/" . $slider), 'title' => $portfolio->name];
                        }
                    }
                    $sitemap->add(URL::to('/'.$portfolio->alias), $portfolio->updated_at, '1.0', 'daily',$images);
                }
                $blogs = \App\Models\BlogsModel::where('status', \App\Models\ChooseModel::STATUS_SHOW)->orderBy('position', 'desc')->get();
                foreach ($blogs as $blog) {
                    $images = [];
                    if(!empty($blog->sliders)){
                        foreach($blog->sliders as $slider){
                            $images[] = ['url' => URL::to("/storage/" . $slider), 'title' => $blog->title];
                        }
                    }
                    $sitemap->add(URL::to('/blogs/'.$blog->alias), $blog->created_at, '1.0', 'daily');
                }
            }
            $sitemap->store('xml', 'sitemap');
        });
        Route::post('/callback', 'MainController@callback');
        Route::post('/callbackJobs', 'MainController@callbackJobs');

        Route::get('/{alias}', 'MainController@portfolio');
    });

Route::post('/update-sub', 'MainController@category');
Route::post('/update-price', 'MainController@price');
Route::post('/get-modal', 'MainController@modal');
Route::post('/get-project', 'MainController@getProject');
