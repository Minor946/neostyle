<?php

namespace App\Admin\Controllers;

use App\Http\Controllers\Controller;
use App\Models\CategoriesModel;
use App\Models\ChooseModel;
use App\Models\PortfolioModel;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;

class PortfolioController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Index')
            ->description('description')
            ->body($this->grid());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new PortfolioModel);

        $grid->id('ID');

        $grid->name('Заголовок');

        $grid->column('category_style', 'Стиль')->display(function () {
            return $this->categoryStyle->name;
        })->sortable();

        $grid->square('Общая площать');
        $grid->location('Местонахождение');
        $grid->position('Позиция');

        $grid->image('Фото')->lightbox(['zooming' => true]);

        $grid->column('status', 'Статус')->display(function () {
            if ($this->status == ChooseModel::STATUS_SHOW ) {
                return "<span class='label label-success'>Показывается</span>";
            }
            if ($this->status == ChooseModel::STATUS_HIDE ) {
                return "<span class='label label-info'>Скрыто</span>";
            }
        })->sortable();

        $grid->created_at('Created at');
        return $grid;
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Detail')
            ->description('description')
            ->body($this->detail($id));
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(PortfolioModel::findOrFail($id));

        $show->id('ID');
        $show->created_at('Created at');
        $show->updated_at('Updated at');

        return $show;
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Edit')
            ->description('description')
            ->body($this->form()->edit($id));
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new PortfolioModel);

        $form->display('ID');

        $form->text('name', 'Название');

        $form->editor('description', 'Описание');

        $form->multipleSelect('categories', 'Родительская')->options(
            CategoriesModel::where('type', CategoriesModel::TYPE_PORTFOLIO)->pluck('name', 'id'));

        $form->select('category_style', 'Стиль')->options(CategoriesModel::where('parent_id', 4)
            ->where('type', CategoriesModel::TYPE_PORTFOLIO)->pluck('name', 'id'));

        $form->text('square', 'Общая площать');
        $form->text('location', 'Местонахождение');
        $form->date('build_at', 'Построенно');

        $status = [
            ChooseModel::STATUS_SHOW => 'Показывать',
            ChooseModel::STATUS_HIDE => 'Скрыть',
        ];

        $form->image('image', 'Изображение проекта')->move('/portfolio')->uniqueName();

        $form->image('panorama', 'Панорамное изображение')->move('/portfolio')->uniqueName();

        $form->multipleImage('sliders', 'Фотографии на слайдер')->move('/portfolio/sliders')->uniqueName()->removable();

        $form->select('status', 'Статус')->options($status);

        $form->number('position', 'Позиция');

        $form->saving(function (Form $form) {
            $form->model()->alias = str_slug($form->name, '_');
        });

        $form->saved(function (Form $form) {
            if (isset($form->image)) {
                $img = Image::make($form->image);
            }
            if (!empty($form->model()->image)) {
                $file = Storage::disk('public')->get($form->model()->image);
                $img = Image::make($file);
            }

            $img->resize(600, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            $name = explode('.', $form->model()->image);
            Storage::put('public/' . $name[0] . '_small.jpg', (string)$img->encode('jpg',95));
        });

        $form->display('Created at');
        $form->display('Updated at');

        return $form;
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Create')
            ->description('description')
            ->body($this->form());
    }
}
