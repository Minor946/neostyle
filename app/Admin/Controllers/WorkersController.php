<?php

namespace App\Admin\Controllers;

use App\Http\Controllers\Controller;
use App\Models\ChooseModel;
use App\Models\WorkersModel;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;

class WorkersController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Index')
            ->description('description')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Detail')
            ->description('description')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Edit')
            ->description('description')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Create')
            ->description('description')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new WorkersModel);

        $grid->id('ID');

        $grid->name('ФИО');

        $grid->specific('Должность');

        $grid->image('Фото')->lightbox(['zooming' => true]);

        $grid->column('status', 'Статус')->display(function () {
            if ($this->status == ChooseModel::STATUS_SHOW) {
                return "<span class='label label-success'>Показывается</span>";
            }
            if ($this->status == ChooseModel::STATUS_HIDE) {
                return "<span class='label label-info'>Скрыто</span>";
            }
        })->sortable();

        $grid->created_at('Created at');
        $grid->updated_at('Updated at');

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(WorkersModel::findOrFail($id));

        $show->id('ID');
        $show->created_at('Created at');
        $show->updated_at('Updated at');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new WorkersModel);

        $form->display('ID');

        $form->text('name', 'ФИО');
        $form->text('specific', 'Должность');

        $status = [
            ChooseModel::STATUS_SHOW => 'Показывать',
            ChooseModel::STATUS_HIDE => 'Скрыть',
        ];
        $form->image('image', 'Фотография')->move('/workers')->uniqueName();


        $form->color('color', "Цвет полоски")->default('#8846f7');

        $form->select('status', 'Статус')->options($status);

        $form->number('position', 'Позиция');

        $form->saved(function (Form $form) {
            if (isset($form->image)) {
                $img = Image::make($form->image);
            }
            if (!empty($form->model()->image)) {
                $file = Storage::disk('public')->get($form->model()->image);
                $img = Image::make($file);
            }

            $img->resize(400, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            $name = explode('.', $form->model()->image);
            Storage::put('public/' . $name[0] . '_small.jpg', (string)$img->encode('jpg', 85));
        });

        $form->display('Created at');
        $form->display('Updated at');

        return $form;
    }
}
