<?php

namespace App\Admin\Controllers;

use App\Http\Controllers\Controller;
use App\Models\ChooseModel;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class ChooseController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Index')
            ->description('description')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Detail')
            ->description('description')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Edit')
            ->description('description')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Create')
            ->description('description')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new ChooseModel);

        $grid->id('ID');

        $grid->title("Заголовок");
        $grid->column('description', 'Текст')->display(function () {
            return $this->description;
        });
        $grid->image('Фото')->lightbox(['zooming' => true]);

        $grid->column('status', 'Статус')->display(function () {
            if ($this->status == ChooseModel::STATUS_SHOW ) {
                return "<span class='label label-success'>Показывается</span>";
            }
            if ($this->status == ChooseModel::STATUS_HIDE ) {
                return "<span class='label label-info'>Скрыто</span>";
            }
        })->sortable();


        $grid->created_at('Создано');

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(ChooseModel::findOrFail($id));

        $show->id('ID');
        $show->created_at('Created at');
        $show->updated_at('Updated at');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new ChooseModel);

        $form->display('ID');

        $form->text('title', 'Заголовок');

        $form->editor('description', 'Описание');

        $status = [
            ChooseModel::STATUS_SHOW => 'Показывать',
            ChooseModel::STATUS_HIDE => 'Скрыть',
        ];

        $form->select('status', 'Статус')->options($status);

        $form->image('image', 'Изображение')->move('/choose')->uniqueName();

        $form->display('Created at');
        $form->display('Updated at');

        return $form;
    }
}
