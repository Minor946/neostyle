<?php

namespace App\Admin\Controllers;

use App\Http\Controllers\Controller;
use App\Models\BlogsModel;
use App\Models\CategoriesModel;
use App\Models\ChooseModel;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;

class BlogsController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Index')
            ->description('description')
            ->body($this->grid());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new BlogsModel);

        $grid->id('ID');

        $grid->title("Заголовок");

        $grid->image('Фото')->lightbox(['zooming' => true]);

        $grid->position("Позиция");

        $grid->column('status', 'Статус')->display(function () {
            if ($this->status == ChooseModel::STATUS_SHOW ) {
                return "<span class='label label-success'>Показывается</span>";
            }
            if ($this->status == ChooseModel::STATUS_HIDE ) {
                return "<span class='label label-info'>Скрыто</span>";
            }
        })->sortable();

        $grid->created_at('Created at');
        return $grid;
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Detail')
            ->description('description')
            ->body($this->detail($id));
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(BlogsModel::findOrFail($id));

        $show->id('ID');
        $show->created_at('Created at');
        $show->updated_at('Updated at');

        return $show;
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Edit')
            ->description('description')
            ->body($this->form()->edit($id));
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new BlogsModel);

        $form->display('ID');

        $form->text('title', 'Заголовок');

        $form->select('type', 'Категория')->options(CategoriesModel::where('type', CategoriesModel::TYPE_BLOG)->pluck('name', 'id'));

        $form->text('item', 'Проект/ссылка');

        $form->image('image', 'Изображение статьи')->move('/portfolio')->uniqueName();


        $form->editor('description', 'Новость');

        $status = [
            ChooseModel::STATUS_SHOW => 'Показывать',
            ChooseModel::STATUS_HIDE => 'Скрыть',
        ];

        $form->multipleImage('sliders', 'Фотографии на слайдер')->move('/post/sliders')->uniqueName()->removable();

        $form->color('color', "Цвет полоски")->default('#8846f7');

        $form->select('status', 'Статус')->options($status);

        $form->number('position', 'Позиция');

        $form->saving(function (Form $form) {
            $form->model()->alias = str_slug($form->title, '_');
        });


        $form->date('created_at', 'Создано');

        $form->display('Updated at');

        $form->saved(function (Form $form) {
            if (isset($form->image)) {
                $img = Image::make($form->image);
            }
            if (!empty($form->model()->image)) {
                $file = Storage::disk('public')->get($form->model()->image);
                $img = Image::make($file);
            }

            $img->resize(400, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            $name = explode('.', $form->model()->image);
            Storage::put('public/' . $name[0] . '_small.jpg', (string)$img->encode('jpg', 85));
        });

        return $form;
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Create')
            ->description('description')
            ->body($this->form());
    }
}
