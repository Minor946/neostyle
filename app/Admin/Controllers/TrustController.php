<?php

namespace App\Admin\Controllers;

use App\Http\Controllers\Controller;
use App\Models\ChooseModel;
use App\Models\PortfolioModel;
use App\Models\TrustModel;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;

class TrustController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Index')
            ->description('description')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Detail')
            ->description('description')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Edit')
            ->description('description')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Create')
            ->description('description')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new TrustModel);

        $grid->column('portfolio_id', 'Проект')->display(function () {
            return $this->portfolio->name;
        })->sortable();

        $grid->image('Фото')->lightbox(['zooming' => true]);

        $grid->column('status', 'Статус')->display(function () {
            if ($this->status == ChooseModel::STATUS_SHOW ) {
                return "<span class='label label-success'>Показывается</span>";
            }
            if ($this->status == ChooseModel::STATUS_HIDE ) {
                return "<span class='label label-info'>Скрыто</span>";
            }
        })->sortable();

        $grid->created_at('Created at');

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(TrustModel::findOrFail($id));

        $show->id('ID');
        $show->created_at('Created at');
        $show->updated_at('Updated at');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new TrustModel);

        $form->display('ID');


        $form->select('portfolio_id', 'Проект')->options(PortfolioModel::pluck('name', 'id'));

        $form->image('image', 'Логотип')->move('/trust')->uniqueName();
        $form->image('image_truest', 'Изображение в нам доевряют')->move('/trust')->uniqueName();


        $form->number('position', 'Позиция');

        $status = [
            ChooseModel::STATUS_SHOW => 'Показывать',
            ChooseModel::STATUS_HIDE => 'Скрыть',
        ];

        $form->select('status', 'Статус')->options($status);

        $form->saved(function (Form $form) {
            if (isset($form->image_truest)) {
                $img = Image::make($form->image_truest);
            }
            if (!empty($form->model()->image_truest)) {
                $file = Storage::disk('public')->get($form->model()->image_truest);
                $img = Image::make($file);
            }

            $img->resize(400, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            $name = explode('.', $form->model()->image_truest);
            Storage::put('public/' . $name[0] . '_small.jpg', (string)$img->encode('jpg', 85));

            if (isset($form->image)) {
                $img = Image::make($form->image);
            }
            if (!empty($form->model()->image)) {
                $file = Storage::disk('public')->get($form->model()->image);
                $img = Image::make($file);
            }

            $img->resize(300, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            $name = explode('.', $form->model()->image);
            Storage::put('public/' . $name[0] . '_small.jpg', (string)$img->encode('jpg', 85));
        });

        $form->display('Created at');
        $form->display('Updated at');

        return $form;
    }
}
