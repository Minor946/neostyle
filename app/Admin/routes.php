<?php

use Illuminate\Routing\Router;

Admin::registerAuthRoutes();

Route::group([
    'prefix'        => config('admin.route.prefix'),
    'namespace'     => config('admin.route.namespace'),
    'middleware'    => config('admin.route.middleware'),
], function (Router $router) {

    $router->get('/', 'HomeController@index');

    Route::resource('choose', 'ChooseController');
    Route::resource('services', 'ServicesController');
    Route::resource('sliders', 'SlidersController');
    Route::resource('categories', 'CategoriesController');
    Route::resource('portfolio', 'PortfolioController');
    Route::resource('partners', 'PartnersController');
    Route::resource('comments', 'CommentsController');
    Route::resource('reviews', 'ReviewsController');
    Route::resource('blogs', 'BlogsController');
    Route::resource('price-list', 'PriceListController');
    Route::resource('trust', 'TrustController');
    Route::resource('rooms', 'PortfolioRoomController');
    Route::resource('pages', 'PagesController');
    Route::resource('vacancy', 'VacancyController');
    Route::resource('workers', 'WorkersController');
    Route::resource('calc', 'ModalCalcController');

    $router->get('/langman', '\Themsaid\LangmanGUI\LangmanController@index');
    $router->post('/langman/scan', '\Themsaid\LangmanGUI\LangmanController@scan');
    $router->post('/langman/save', '\Themsaid\LangmanGUI\LangmanController@save');
    $router->post('/langman/add-language', '\Themsaid\LangmanGUI\LangmanController@addLanguage');
});
