<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $name
 * @property string $requirements
 * @property string $responsibility
 * @property string $conditions
 * @property string $personal
 * @property int $status
 * @property string $created_at
 * @property string $updated_at
 */
class VacancyModel extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'vacancy';

    /**
     * @var array
     */
    protected $fillable = ['name', 'requirements', 'responsibility', 'conditions', 'personal', 'status', 'created_at', 'updated_at'];

}
