<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $name
 * @property string $short_desc
 * @property string $url
 * @property string $poster
 * @property string $status
 * @property string $created_at
 * @property string $updated_at
 */
class ReviewsModel extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'reviews';

    /**
     * @var array
     */
    protected $fillable = ['name', 'short_desc', 'url', 'poster', 'status', 'created_at', 'updated_at'];

}
