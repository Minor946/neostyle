<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $name
 * @property string $specific
 * @property string $image
 * @property string $color
 * @property int $status
 * @property int $position
 * @property string $created_at
 * @property string $updated_at
 */

class WorkersModel extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'workers';

    /**
     * @var array
     */
    protected $fillable = ['name', 'specific', 'image', 'color', 'position', 'status', 'created_at', 'updated_at'];

}
