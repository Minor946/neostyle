<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $title_h3
 * @property string $title_h2
 * @property string $description
 * @property string $color
 * @property string $image
 * @property int $position
 * @property int $status
 * @property string $created_at
 * @property string $updated_at
 */
class SlidersModel extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'sliders';

    /**
     * @var array
     */
    protected $fillable = ['title_h3', 'title_h2', 'description', 'color', 'image', 'position', 'status', 'created_at', 'updated_at'];

}
