<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $name
 * @property string $alias
 * @property string $image
 * @property string $categories
 * @property int $category_style
 * @property string $square
 * @property string $location
 * @property string $build_at
 * @property string $description
 * @property string $sliders
 * @property string $panorama
 * @property int $status
 * @property int $position
 * @property string $created_at
 * @property string $updated_at
 */
class PortfolioModel extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'portfolio';

    /**
     * @var array
     */
    protected $fillable = ['name', 'alias', 'image','panorama', 'categories', 'category_style', 'square', 'location', 'build_at', 'description', 'sliders', 'status', 'position', 'created_at', 'updated_at'];


    public function getCategoriesAttribute($value)
    {
        return explode(',', $value);
    }

    public function setCategoriesAttribute($value)
    {
        $this->attributes['categories'] = implode(',', $value);
    }

    public function setSlidersAttribute($sliders)
    {
        if (is_array($sliders)) {
            $this->attributes['sliders'] = json_encode($sliders);
        }
    }

    public function getSlidersAttribute($sliders)
    {
        return json_decode($sliders, true);
    }

    public function categoryStyle()
    {
        return $this->belongsTo('App\Models\CategoriesModel', 'category_style');
    }

    public static function whereAlias($alias)
    {
        $portfolio = PortfolioModel::where('alias', $alias)->first();
        if (!empty($portfolio)) {
            return $portfolio;
        }
        return null;
    }

    public function rooms()
    {
        return $this->hasMany('App\Models\PortfolioRoomModel', 'portfolio_id');
    }

    public function getNext()
    {

        $next = PortfolioModel::where('id', '>', $this->id)->min('id');
        if (empty($next)) {
            $next = PortfolioModel::where('id', '<', $this->id)->max('id');
        }
        return $next;
    }
}
