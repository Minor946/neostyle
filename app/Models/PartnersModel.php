<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $name
 * @property string $image
 * @property string $image2
 * @property string $description
 * @property string $address
 * @property string $site
 * @property string $facebook
 * @property string $instagram
 * @property int $position
 * @property int $status
 * @property int $trust
 * @property string $image_truest
 * @property int $portfolio_id
 * @property string $created_at
 * @property string $updated_at
 */
class PartnersModel extends Model
{
    const TRUST_ON = 1;
    const TRUST_OFF = 0;

    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'partners';

    /**
     * @var array
     */
    protected $fillable = ['name', 'image', 'image2', 'description', 'address', 'site', 'facebook', 'instagram', 'position', 'status', 'trust', 'image_truest', 'portfolio_id', 'created_at', 'updated_at'];

    public function portfolio()
    {
        return $this->belongsTo('App\Models\PortfolioModel', 'portfolio_id');
    }
}
