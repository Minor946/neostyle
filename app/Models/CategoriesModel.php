<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $name
 * @property string $parent_id
 * @property int $type
 * @property int $status
 * @property int $position
 * @property string $created_at
 * @property string $updated_at
 */
class CategoriesModel extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'categories';

    /**
     * @var array
     */
    protected $fillable = ['name', 'parent_id', 'type', 'status', 'position', 'created_at', 'updated_at'];

    const TYPE_PORTFOLIO = 0;
    const TYPE_BLOG = 1;

    public function getParentIdAttribute($value)
    {
        return explode(',', $value);
    }
    public function setParentIdAttribute($value)
    {
        $this->attributes['parent_id'] = implode(',', $value);
    }

    public static function getTypeList(){
        return [
            self::TYPE_PORTFOLIO => "Портфолио",
            self::TYPE_BLOG => "Новость",
        ];
    }

    public function parentCategory()
    {
        return $this->belongsTo('App\Models\CategoriesModel', 'parent_id');
    }
}
