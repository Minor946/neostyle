<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $portfolio_id
 * @property string $name
 * @property string $color
 * @property string $description
 * @property string $images
 * @property int $status
 * @property int $position
 * @property string $created_at
 * @property string $updated_at
 */
class PortfolioRoomModel extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'portfolio_room';

    /**
     * @var array
     */
    protected $fillable = ['id', 'portfolio_id', 'name', 'color', 'description', 'images', 'status', 'position', 'created_at', 'updated_at'];


    public function setImagesAttribute($sliders)
    {
        if (is_array($sliders)) {
            $this->attributes['images'] = json_encode($sliders);
        }
    }

    public function getImagesAttribute($sliders)
    {
        return json_decode($sliders, true);
    }

    public function portfolio()
    {
        return $this->belongsTo('App\Models\PortfolioModel', 'portfolio_id');
    }
}
