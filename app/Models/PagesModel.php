<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $name
 * @property string $alias
 * @property int $type
 * @property int $status
 * @property string $description
 * @property string $created_at
 * @property string $updated_at
 */
class PagesModel extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'pages';

    /**
     * @var array
     */
    protected $fillable = ['name', 'alias', 'type', 'status', 'description', 'created_at', 'updated_at'];

    public static function findByAlias($alias)
    {
        $page = self::where('alias', $alias)->first();
        if(!empty($page)){
            return $page;
        }
        return null;
    }
}
