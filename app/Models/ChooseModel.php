<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $title
 * @property string $description
 * @property string $image
 * @property int $status
 * @property string $created_at
 * @property string $updated_at
 */
class ChooseModel extends Model
{

    const STATUS_SHOW = 1;
    const STATUS_HIDE = 2;
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'choose';

    /**
     * @var array
     */
    protected $fillable = ['title', 'description', 'image', 'status', 'created_at', 'updated_at'];

}
