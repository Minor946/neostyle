<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ModalCalcModel extends Model
{
    protected $table = 'modal_calc';

    public function getValuesAttribute($value)
    {
        return explode(',', $value);
    }

    public function setValuesAttribute($value)
    {
        $this->attributes['values'] = implode(',', $value);
    }

}
