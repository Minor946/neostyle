<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $title
 * @property string $short_desc
 * @property string $alias
 * @property int $type
 * @property string $item
 * @property string $color
 * @property string $image
 * @property string $sliders
 * @property string $description
 * @property int $status
 * @property int $position
 * @property string $created_at
 * @property string $updated_at
 */

class BlogsModel extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'blogs';

    /**
     * @var array
     */
    protected $fillable = ['title', 'short_desc', 'alias', 'type', 'item', 'color', 'image', 'sliders', 'description', 'status', 'position', 'created_at', 'updated_at'];

    public function setSlidersAttribute($sliders)
    {
        if (is_array($sliders)) {
            $this->attributes['sliders'] = json_encode($sliders);
        }
    }

    public function getSlidersAttribute($sliders)
    {
        return json_decode($sliders, true);
    }

    public static function whereAlias($alias)
    {
        $blog = BlogsModel::where('alias', $alias)->first();
        if (!empty($blog)) {
            return $blog;
        }
        return null;
    }

    public function category()
    {
        return $this->belongsTo('App\Models\CategoriesModel', 'type');
    }
}
