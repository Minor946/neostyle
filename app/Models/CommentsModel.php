<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $username
 * @property string $placeholder
 * @property string $comment
 * @property int $status
 * @property string $image
 * @property string $color
 * @property string $created_at
 * @property string $updated_at
 */
class CommentsModel extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'comments';

    /**
     * @var array
     */
    protected $fillable = ['username', 'placeholder', 'comment', 'status', 'image', 'color', 'created_at', 'updated_at'];

}
