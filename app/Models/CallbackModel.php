<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $name
 * @property string $phone
 * @property string $email
 * @property string $type
 * @property string $value
 * @property string $message
 * @property int $status
 * @property string $created_at
 * @property string $updated_at
 */
class CallbackModel extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'callback';

    /**
     * @var array
     */
    protected $fillable = ['name', 'phone', 'email', 'type', 'value', 'message', 'status', 'created_at', 'updated_at'];

}
