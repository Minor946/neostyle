<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $image_truest
 * @property string $image
 * @property int $portfolio_id
 * @property int $status
 * @property int $position
 * @property string $created_at
 * @property string $updated_at
 */
class TrustModel extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'trust';

    /**
     * @var array
     */
    protected $fillable = ['image_truest', 'image', 'portfolio_id', 'status', 'position', 'created_at', 'updated_at'];

    public function portfolio()
    {
        return $this->belongsTo('App\Models\PortfolioModel', 'portfolio_id');
    }
}
