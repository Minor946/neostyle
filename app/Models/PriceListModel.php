<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $name
 * @property int $type
 * @property int $plan
 * @property string $created_at
 * @property string $updated_at
 */
class PriceListModel extends Model
{
    const TYPE_DESIGN = 0;
    const TYPE_REPAIR = 1;
    const TYPE_ARCHITECT= 2;
    const TYPE_LANDSCAPE = 3;

    const PLAN_ECONOMY = 0;
    const PLAN_COMFORT = 1;
    const PLAN_MAXIMUM = 2;
    const PLAN_PREMIUM = 3;

    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'price_list';

    /**
     * @var array
     */
    protected $fillable = ['name', 'type', 'plan', 'created_at', 'updated_at'];


    public static function getListType(){
        return[
            self::TYPE_DESIGN => "Дизайн интерьера",
            self::TYPE_REPAIR => "Ремонт и отделка",
            self::TYPE_ARCHITECT => "Архитектурное проектирование",
            self::TYPE_LANDSCAPE => "Ландшафтный дизайн"
        ];
    }

    public static function getListPlan(){
        return[
            self::PLAN_ECONOMY => "Эконом",
            self::PLAN_COMFORT => "Комфорт",
            self::PLAN_MAXIMUM => "Максимум",
            self::PLAN_PREMIUM => "Премиум"
        ];
    }

    public function getTypeName(){
        $type = self::getListType();
        return $type[$this->type];
    }

    public function getPlanName(){
        $type = self::getListPlan();
        return $type[$this->plan];
    }
}
