<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $type
 * @property string $point
 * @property int $status
 * @property string $created_at
 * @property string $updated_at
 */
class ServicesModel extends Model
{
    const TYPE_DESIGN = 0;
    const TYPE_REPAIR = 1;
    const TYPE_ARCHITECT= 2;
    const TYPE_LANDSCAPE = 3;

    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'services';

    /**
     * @var array
     */
    protected $fillable = ['type', 'point', 'status', 'created_at', 'updated_at'];


    public static function getListType(){
        return[
            self::TYPE_DESIGN => "Дизайн интерьера",
            self::TYPE_REPAIR => "Ремонт и отделка",
            self::TYPE_ARCHITECT => "Архитектурное проектирование",
            self::TYPE_LANDSCAPE => "Ландшафтный дизайн"
        ];
    }
}
