<?php
/**
 * Created by PhpStorm.
 * User: Minor946
 * Date: 21.01.2019
 * Time: 13:33
 */

namespace App\Providers;


use Illuminate\Support\ServiceProvider;

class ConfigServiceProvider extends ServiceProvider
{
    public function register()
    {
        config([
            'laravellocalization.supportedLocales' => [
                'ru' => ['name' => 'Russian', 'short' => 'RUS', 'script' => 'Cyrl', 'native' => 'русский', 'regional' => 'ru_RU'],
                'ky' => ['name' => 'Kyrgyz', 'script' => 'Cyrl', 'native' => 'Кыргыз', 'regional' => 'ky_KG'],
            ],

            'localesOrder.supportedLocales' => ['ru','ky'],

            'laravellocalization.useAcceptLanguageHeader' => true,
            'laravellocalization.hideDefaultLocaleInURL' => true
        ]);
    }

}