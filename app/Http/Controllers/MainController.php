<?php
/**
 * Created by PhpStorm.
 * User: lolbu
 * Date: 23.09.2018
 * Time: 19:03
 */

namespace App\Http\Controllers;


use App\Models\Blogs;
use App\Models\BlogsModel;
use App\Models\CallbackModel;
use App\Models\Callbacks;
use App\Models\PagesModel;
use App\Models\PartnersModel;
use App\Models\PortfolioModel;
use App\Models\PriceListModel;
use App\Models\SlidersModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class MainController extends Controller
{

    const TYPE_MODAL_PARTNERS = 0;

    public function index()
    {
        return view('welcome');
    }

    public function blog($alias)
    {
        $model = BlogsModel::whereAlias($alias);
        if (!empty($model)) {
            return view('blog', ['model' => $model]);
        } else {
            return abort(404);
        }
    }

    public function blogs()
    {
        return view('blogs');
    }

    public function actions()
    {
        return view('discount');
    }

    public function portfolioList()
    {
        return view('portfolioList');
    }

    public function contacts()
    {
        return view('contacts');
    }

    public function jobs()
    {
        return view('jobs');
    }

    public function thanks()
    {
        return view('thanks');
    }

    public function partners()
    {
        return view('partners');
    }


    public function portfolio($alias)
    {
        $model = PortfolioModel::whereAlias($alias);
        if (!empty($model)) {
            return view('portfolio', ['model' => $model]);
        } else {
            return abort(404);
        }
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function category(Request $request)
    {
        $data = $request->all();
        if (!empty($data) && $request->isMethod('post')) {
            $value = $request->input('category');

            $categories = \App\Models\CategoriesModel::whereRaw('FIND_IN_SET(' . $value . ' , parent_id) AND type = 0 ')
                ->orderBy('position', 'asc')->get();

            if (count($categories) > 0) {
                $out = "";
                foreach ($categories as $category) {
                    $out .= view('components.items.category_item', ['category' => $category, 'parent_id' => $value]);
                }
                $out .= view('components.items.category_item', ['category' => null, 'parent_id' => $value]);
                return response()->json(['data' => $out]);
            }
        }
        return response()->json(null);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function price(Request $request)
    {
        $data = $request->all();
        if (!empty($data) && $request->isMethod('post')) {
            $type = $request->input('type');
            $plan = $request->input('plan');

            $price_list = \App\Models\PriceListModel::where('type', $type)->where('plan', $plan)->get();

            if (count($price_list) > 0) {
                $out = "";
                foreach ($price_list as $item) {
                    $out .= view('components.items.price_list_item', ['item' => $item->name]);
                }
                return response()->json(['data' => $out]);
            }
        }
        return response()->json(null);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function modal(Request $request)
    {
        $data = $request->all();
        if (!empty($data) && $request->isMethod('post')) {
            $type = $request->input('type');
            $out = "";

            if ($type == self::TYPE_MODAL_PARTNERS) {
                $value = $request->input('item');
                if (!empty($value)) {
                    $model = PartnersModel::find($value);
                    $out .= view('modals.modal_partners', ['model' => $model->toJson()])->render();
                    return response()->json(['data' => $out]);
                }
            }
        }
        return response()->json(null);
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function getProject(Request $request)
    {
        $data = $request->all();
        if (!empty($data) && $request->isMethod('post')) {
            $parent = $request->input('parent');
            $sub_category = $request->input('sub_category');
            $out = "";
            if (!empty($parent)) {
                if ($parent == "*") {
                    $portfolio = \App\Models\PortfolioModel::where('status', \App\Models\ChooseModel::STATUS_SHOW)
                        ->orderBy(DB::raw('RAND()'))->take(6)->get();
                } elseif (!empty($sub_category) && ($parent == 4)) {
                    if ($sub_category == '*' || empty($sub_category)) {
                        $portfolio = \App\Models\PortfolioModel::where('status', \App\Models\ChooseModel::STATUS_SHOW)
                            ->orderBy('position', 'desc')->take(6)->get();
                    } else {
                        $portfolio = \App\Models\PortfolioModel::where('status', \App\Models\ChooseModel::STATUS_SHOW)
                            ->whereRaw('category_style = ' . $sub_category)
                            ->orderBy('position', 'desc')->take(6)->get();
                    }
                } else {
                    if ($sub_category == "*") {
                        $portfolio = \App\Models\PortfolioModel::where('status', \App\Models\ChooseModel::STATUS_SHOW)
                            ->whereRaw('FIND_IN_SET(?, categories)', [$parent])
                            ->orderBy('position', 'desc')->take(6)->get();
                    } else {
                        $portfolio = \App\Models\PortfolioModel::where('status', \App\Models\ChooseModel::STATUS_SHOW)
                            ->whereRaw('FIND_IN_SET(?, categories) AND FIND_IN_SET(' . $sub_category . ', categories)', [$parent])
                            ->orderBy('position', 'desc')->take(6)->get();
                    }
                }
                foreach ($portfolio as $project) {
                    $out .= (String)view('components.items.portfolio_item', ['project' => $project]);
                }
                return response()->json(['data' => $out]);
            }
        }
        return response()->json(null);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function callbackJobs(Request $request)
    {
        $link = $request->input('link');
        $file = $request->file('file');

        $model = new CallbackModel();
        $model->name = $link;

        Mail::send('emails.portfolio', ['callback' => $model], function ($m) use ($file, $model) {
            $m->from('support@neostyle.kg', 'NeoStyle.kg');
            $m->to('job@neostyle.kg', 'NeoStyle.kg');
            $m->attach($file->getRealPath(), array(
                    'as' => $file->getClientOriginalName(),
                    'mime' => $file->getMimeType())
            );
            $m->subject("Новое резюме");
        });
        return redirect('thanks');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function callback(Request $request)
    {
        $name = $request->input('name', '');
        $type = $request->input('type', 'not_set');
        $email = $request->input('email', '');
        $phone = $request->input('phone', '');
        $message = $request->input('message', '');
        $valueSquare = $request->input('valueSquare', "");
        $valueType = $request->input('valueType', "");
        $value = $request->input('value', "");

        $model = new CallbackModel();

        $model->name = $name;
        $model->type = $type;
        $model->email = $email;
        $model->phone = $phone;
        $model->message = $message;
        if (!empty($valueSquare) && !empty($value)) {
            if(!empty(strpos($value, '|') !== false)) {
                $param = explode('|', $value);
                if (is_array($param)) {
                    $plan = PriceListModel::getListPlan();
                    $type = PriceListModel::getListType();
                    $value = $type[$param[0]] . " " . $plan[$param[1]];
                }
            }
            $model->value = $valueSquare . " м2 |" . $valueType . "|" . $value;
        } else {
            if(!empty(strpos($value, '|') !== false)){
                $param = explode('|',$value);
                if(is_array($param)){
                    $plan = PriceListModel::getListPlan();
                    $type = PriceListModel::getListType();
                    $value = $type[$param[0]]. " ".$plan[$param[1]];
                }
            }
            if( $model->type == 'form-discount'){
                $value = SlidersModel::find($value)->title_h3;
            }
            $model->value = $value;
        }
        $model->status = 0;

        $model->save();

        Mail::send('emails.callback', ['callback' => $model], function ($m) use ($model) {
            $m->from('support@neostyle.kg', 'NeoStyle.kg');
            $m->to('zayavka@neostyle.kg', 'NeoStyle.kg')
                ->subject("Новый запрос");
        });

        return redirect('thanks');
    }

    public function agreement()
    {
        $model = PagesModel::find(1);
        return view('page', ['model' => $model]);
    }

    public function policy()
    {
        $model = PagesModel::find(2);
        return view('page', ['model' => $model]);
    }

    public function pages($alias)
    {
        $model = PagesModel::findByAlias($alias);
        if (!empty($model))
            return view('page', ['model' => $model]);

        return abort(404);
    }

}