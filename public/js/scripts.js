var isMobile = false; //initiate as false
// device detection
if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent)
    || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0, 4))) {
    isMobile = true;
}
var myLazyLoad = new LazyLoad({
    elements_selector: ".lazy"
});

var gridLimit = 9999;

var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
    sURLVariables = sPageURL.split('&'),
    sParameterName,
    i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
};

var parentMain = getUrlParameter('parent');
var sub_category = getUrlParameter('sub_category');

function gridBlog() {
    var $grid = $('#portfolio-project').isotope({
        itemSelector: '.portfolio-item',
        hiddenStyle: {
            opacity: 0
        },
        visibleStyle: {
            opacity: 1
        },
        masonry: {
            columnWidth: 370,
            gutter: 10,
            horizontalOrder: true,
            fitWidth: true
        },
    });
    if (parentMain) {
        if (parentMain === '4') {
            if (sub_category === '*') {
                console.log('*');
                $grid.isotope({
                    filter: function () {
                        return $(this).is('.portfolio-item') && $(this).index('.portfolio-item') < gridLimit;
                    }
                });
            } else {
                console.log('sub_category');
                $grid.isotope({
                    filter: function () {
                        return $(this).is('.portfolio-style-' + sub_category) && $(this).index('.portfolio-style-' + sub_category) < gridLimit;
                    }
                });
            }
        } else if (sub_category === '*') {
            console.log('parent');
            $grid.isotope({
                filter: function () {
                    return $(this).is('.portfolio-category-' + parentMain) && $(this).index('.portfolio-category-' + parentMain) < gridLimit;
                }
            });
        } else {
            console.log('parent + sub_category');
            $grid.isotope({
                filter: function () {
                    return $(this).is('.portfolio-category-' + sub_category + '.portfolio-category-' + parentMain) && $(this).index('.portfolio-category-' + sub_category + '.portfolio-category-' + parentMain) < gridLimit;
                }
            });
        }
    }

    $(".nav-portfolio-category").on('click', function () {
        var value = $(this).attr("data-value");
        $(".portfolio-item.hidden").removeClass('hidden');
        if (value === '4') {
            $grid.isotope({
                filter: function () {
                    return $(this).is('.portfolio-item') && $(this).index('.portfolio-item') < gridLimit;
                }
            });
            updateSubCategory(value);
        } else if (value === '*') {
            $grid.isotope({
                filter: function () {
                    return $(this).is('.portfolio-item') && $(this).index('.portfolio-item') < gridLimit;
                }
            });
            $(".portfolio-sub-categories").html("");
        } else {
            $grid.isotope({
                filter: function () {
                    return $(this).is('.portfolio-category-' + value) && $(this).index('.portfolio-category-' + value) < gridLimit;
                }
            });
            updateSubCategory(value);
        }
    });

    $(document).on('click', ".btn-sub-category", function () {
        $(".portfolio-item.hidden").removeClass('hidden');
        $(".btn-sub-category").removeClass('active');
        $(this).addClass('active');
        var value = $(this).attr("data-value");
        var parent = $(this).attr("data-parent");
        if (parent === '4') {
            if (value === '*') {
                $grid.isotope({
                    filter: function () {
                        return $(this).is('.portfolio-item') && $(this).index('.portfolio-item') < gridLimit;
                    }
                });
            } else {
                $grid.isotope({
                    filter: function () {
                        return $(this).is('.portfolio-style-' + value) && $(this).index('.portfolio-style-' + value) < gridLimit;
                    }
                });
            }
        } else if (value === '*') {
            $grid.isotope({
                filter: function () {
                    return $(this).is('.portfolio-category-' + parent) && $(this).index('.portfolio-category-' + parent) < gridLimit;
                }
            });
        } else {
            $grid.isotope({
                filter: function () {
                    return $(this).is('.portfolio-category-' + value + '.portfolio-category-' + parent) && $(this).index('.portfolio-category-' + value + '.portfolio-category-' + parent) < gridLimit;
                }
            });
        }
    });
}

function gridMain() {
    var size_item = 370;
    var ww = $(window).width();
    if (ww > 575 && ww <= 768) {
        size_item = 350;
    }

    var $grid = $('#portfolio-project').isotope({
        itemSelector: '.portfolio-item',
        hiddenStyle: {
            opacity: 0
        },
        visibleStyle: {
            opacity: 1
        },
        masonry: {
            columnWidth: size_item,
            gutter: 10,
            horizontalOrder: true,
            fitWidth: true
        },
    });

    $(".nav-portfolio-category").on('click', function () {
        var value = $(this).attr("data-value");
        addProject(value, '*');
    });

    function addProject(value, sub_category) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var btn = $('#btn-all-portfolio');
        btn.attr('href', '/portfolio?parent=' + value + '&sub_category=' + sub_category);

        $.ajax({
            url: '/get-project',
            method: 'POST',
            cache: false,
            data: {parent: value, sub_category: sub_category},
            beforeSend: function () {
                inProgress = true;
            }
        }).done(function (data) {
            $('#portfolio-project').html("");
            $grid.prepend(data.data)
            .isotope('prepended', data.data)
            .isotope('reloadItems');
            inProgress = false;
            if (sub_category === '*') {
                filterGrid(value)
            } else {
                filterGrid(value)
            }
        });
    }

    function filterGrid(value) {
        $('html, body').animate({
            scrollTop: $('#nav-home').offset().top - 150
        }, 500);

        $(".portfolio-item.hidden").removeClass('hidden');
        if (value === '4') {
            $grid.isotope({
                filter: function () {
                    return $(this).is('.portfolio-item') && $(this).index('.portfolio-item') < gridLimit;
                }
            });
            updateSubCategory(value);
        } else if (value === '*') {
            $grid.isotope({
                filter: function () {
                    return $(this).is('.portfolio-item') && $(this).index('.portfolio-item') < gridLimit;
                }
            });
            $(".portfolio-sub-categories").html("");
        } else {
            $grid.isotope({
                filter: function () {
                    return $(this).is('.portfolio-category-' + value) && $(this).index('.portfolio-category-' + value) < gridLimit;
                }
            });
            updateSubCategory(value);
        }
    }

    $(document).on('click', ".btn-sub-category", function () {
        $(".portfolio-item.hidden").removeClass('hidden');
        $(".btn-sub-category").removeClass('active');
        $(this).addClass('active');
        var value = $(this).attr("data-value");
        var parent = $(this).attr("data-parent");
        $('#portfolio-project').html("");
        addProject(parent, value);
    });
}

function updateSubCategory(value) {
    var inProgress = false;
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        url: '/update-sub',
        method: 'POST',
        cache: false,
        data: {category: value},
        beforeSend: function () {
            inProgress = true;
        }
    }).done(function (data) {
        if (data.data == null) {
            $(".portfolio-sub-categories").html("");
        } else {
            $(".portfolio-sub-categories").html(data.data);
        }
        inProgress = false;
    });
}

$(document).ready(function ($) {

    if (window.location.pathname === '/') {
        gridLimit = 6;
    }

    $(document).on('click', 'a[href*="#"]', function (event) {
        if ($($.attr(this, 'href')) && (!$(this).hasClass('nav-price') || !$(this).hasClass('nav-job'))) {
            if ($(this).attr('href') != '#previous' && !$(this).attr('href').includes('step')) {
                event.preventDefault();
                if ($('#navbarNavAltMarkup').length) {
                    $('#navbarNavAltMarkup').removeClass('show')
                }
                if ($(".hamburger").hasClass('is-active')) {
                    $(".hamburger").removeClass('is-active')
                }
                $('html, body').animate({
                    scrollTop: $($.attr(this, 'href')).offset().top - 54
                }, 500);
            }
        }
    });

    $(window).scroll(function () {
        var height = $(window).scrollTop();
        if (height >= 99) {
            $('#navbar-second').addClass('fixed-top');
        } else {
            $('#navbar-second').removeClass('fixed-top');
        }
    });

    $(document).ready(function () {
        $('#scrollup').on('click', function () {
            $('html, body').animate({scrollTop: 0}, 500);
        });

        $(window).scroll(function () {
            if ($(document).scrollTop() > 0) {
                $('#scrollup').fadeIn('slow');
            } else {
                $('#scrollup').fadeOut('slow');
            }
        });
    });

    $(".hamburger").on('click', function () {
        $(this).toggleClass('is-active')
    });

    $(".dropdown > li").on('click', function () {
        var $container = $(this),
        $list = $container.find("ul"),
        $anchor = $container.find("a");
        if (!$list.is(":visible") && $list.is(":hidden")) {
            $anchor.addClass("hover");
            $list
            .show()
            .css({
                paddingTop: $container.data("origHeight")
            });
        } else {
            $list.hide();
            $anchor.removeClass("hover");
        }
    });

    $(".dropdown > li").hover(function () {

        var $container = $(this),
        $list = $container.find("ul"),
        $anchor = $container.find("a");
        $container.data("origHeight", 54);
        $anchor.addClass("hover");
        $list
        .show()
        .css({});
    }, function () {

        var $el = $(this);

        $el
        .height($(this).data("origHeight"))
        .find("ul")
        .css({top: 0})
        .hide()
        .end()
        .find("a")
        .removeClass("hover");

    });

    var blogSlider = new Swiper('#blogSlider', {
        loop: true,
        slidesPerView: 3,
        spaceBetween: 0,
        slidesOffsetBefore: 25,
        navigation: {
            nextEl: '.blog-button-next',
            prevEl: '.blog-button-prev',
        },
        pagination: {
            el: '.blog-pagination',
            clickable: true,
        },
    });

    var moreProject = new Swiper('#moreProject', {
        loop: false,
        slidesPerView: 3,
        spaceBetween: 0,
        navigation: {
            nextEl: '.blog-button-next',
            prevEl: '.blog-button-prev',
        },
        pagination: {
            el: '.blog-pagination',
            clickable: true,
        },
    });

    var projectSlider = new Swiper('#projectSlider', {
        loop: true,
        autoplay: 600,
        spaceBetween: 0,
        navigation: {
            nextEl: '.portfolio-button-next',
            prevEl: '.portfolio-button-prev',
        },
        pagination: {
            el: '.portfolio-pagination',
            clickable: true,
        },
    });


    var videoSlider = new Swiper('#videoSlider', {
        loop: true,
        dynamicBullets: true,
        slidesPerView: 3,
        spaceBetween: 0,
        navigation: {
            nextEl: '.video-button-next',
            prevEl: '.video-button-prev',
        },
        pagination: {
            el: '.video-pagination',
            clickable: true,
        },
    });

    var reviewSlider = new Swiper('#reviewSlider', {
        loop: true,
        dynamicBullets: true,
        slidesPerView: 2,
        spaceBetween: 0,
        navigation: {
            nextEl: '.review-button-next',
            prevEl: '.review-button-prev',
        },
        pagination: {
            el: '.review-pagination',
            clickable: true,
        },
    });

    var chooseSlider = new Swiper('#chooseSlider', {
        loop: true,
        dynamicBullets: true,
        slidesPerView: 3,
        spaceBetween: 0,
        pagination: {
            el: '.choose-pagination',
            clickable: true,
        },
        navigation: {
            nextEl: '.choose-button-next',
            prevEl: '.choose-button-prev',
        },
    });

    var workersSlider = new Swiper('#workersSlider', {
        dynamicBullets: true,
        slidesPerView: 4,
        pagination: {
            el: '.work-pagination',
            clickable: true,
        },
        navigation: {
            nextEl: '.work-button-next',
            prevEl: '.work-button-prev',
        },
    });

    var partnersModalSlider = null;
    var isInitSlider = false;

    $("#layoutsModal").on('shown.bs.modal', function () {
        if (!isInitSlider) {
            partnersModalSlider = new Swiper('#partnersModalSlider', {
                spaceBetween: 0,
                pagination: {
                    el: '.partners-pagination',
                    clickable: true,
                },
                navigation: {
                    nextEl: '.partners-button-next',
                    prevEl: '.partners-button-prev',
                },
            });
            isInitSlider = true;
        }

    });
    $(".btn-partners").on('click', function () {
        var slide = $(this).attr("data-slide");
        if (partnersModalSlider) {
            partnersModalSlider.slideTo(slide);
            partnersModalSlider.update();
        }
    });

    var mainSlider = new Swiper('#mainSlider', {
        loop: true,
        speed: 1000,
        autoplay: {
            delay: 10000,
            disableOnInteraction: false,
        },
        fadeEffect: {
            crossFade: true
        },
        pagination: {
            el: '.main-pagination',
            clickable: true,
            renderBullet: function (index, className) {
                return '<span class="' + className + '">' + (index + 1) + '</span>';
            },
        },
        navigation: {
            nextEl: '.main-button-next',
            prevEl: '.main-button-prev',
        },
    });


    $('a.nav-price[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        var type = $(this).attr("data-type");
        if (type === '3') {
            $('.price-type-item-0').hide();
            $('.price-type-item-2').hide();
        } else {
            $('.price-type-item-0').show();
            $('.price-type-item-2').show();
        }
        $.each($('body').find('.price-more'), function (i, v) {
            $(this).attr("data-type", type);
            $('.btn-price').attr("data-type", type);
            $('.btn-price').attr("data-type", type);
            if (type === '3') {
                $(".price-more[data-plan='1'][data-type='3']").click();
            } else {
                $(".price-more[data-plan='0'][data-type='" + type + "']").click();
            }
        });
        $(".price-description").removeClass('opacity');
        if (isMobile) {
            $('html, body').animate({
                scrollTop: $('.tab-price-content').offset().top - 100
            }, 500);
        }
    });

    $("#price-close").on('click', function () {
        $(".price-description").toggleClass('opacity');
    });

    $(".price-more").on('click', function () {
        var plan = $(this).attr("data-plan");
        var type = $(this).attr("data-type");
        $('.price-type-item.active').removeClass('active');
        $('.price-type-item[data-plan="' + plan + '"]').addClass('active');

        $(".btn-price").attr({
            "data-plan": plan,
            "data-type": type
        });
        updatePriceList(type, plan);
        if (isMobile) {
            $(".price-description").toggleClass('opacity');
        }
    });
$('.url-source').val(window.location.href);
$('form').on('submit', function(){
    let form_name = $(this).find('input[name="type"]').val();
                switch(form_name) {
                    case 'main-question-form':
                        gtag('event', 'main-question-form', {'event_category': 'Lead', 'event_label': 'Остались вопросы (главная страница)'});
                        yaCounter26307888.reachGoal('Leads');
                        break;                    
                    case 'subscribe-form':
                        gtag('event', 'subscribe-form', {'event_category': 'Lead', 'event_label': 'Подписать на новости'});
                        yaCounter26307888.reachGoal('Leads');
                        break;                        
                    case 'first-form':
                        gtag('event', 'first-form', {'event_category': 'Lead', 'event_label': 'Бесплатная консультация'});
                        yaCounter26307888.reachGoal('Leads');
                        break;                        
                    case 'checkList-form':
                        gtag('event', 'checkList-form', {'event_category': 'Lead', 'event_label': 'Скачать чеклист'});
                        yaCounter26307888.reachGoal('Leads');
                        break;                    
                    case 'form-action':
                        gtag('event', 'form-action', {'event_category': 'Lead', 'event_label': 'Узнай стоимость ремонта'});
                        yaCounter26307888.reachGoal('Leads');
                        break;                    
                    case 'form-price':
                        gtag('event', 'form-price', {'event_category': 'Lead', 'event_label': 'Стоимость услуг'});
                        yaCounter26307888.reachGoal('Leads');
                        break;                    
                    case 'form-query':
                        gtag('event', 'form-query', {'event_category': 'Lead', 'event_label': 'Оставить запрос'});
                        yaCounter26307888.reachGoal('Leads');
                        break;                    
                    case 'form-review':
                        gtag('event', 'form-review', {'event_category': 'Lead', 'event_label': 'Отзыв'});
                        yaCounter26307888.reachGoal('Leads');
                        break;                    
                    case 'form-discount':
                        gtag('event', 'form-discount', {'event_category': 'Lead', 'event_label': 'Участвовать в акции'});
                        yaCounter26307888.reachGoal('Leads');
                        break;
                    }
});
    function updatePriceList(type, plan) {
        var inProgress = false;
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: '/update-price',
            method: 'POST',
            cache: false,
            data: {type: type, plan: plan},
            beforeSend: function () {
                inProgress = true;
            }
        }).done(function (data) {
            $("#priceList").html(data.data);
            inProgress = false;
        });
    }


// $(".btn-partners").on('click', function () {
//     var type = $(this).attr("data-type");
//     var item = $(this).attr("data-value");
//     var inProgress = false;
//     $.ajaxSetup({
//         headers: {
//             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//         }
//     });
//     $.ajax({
//         url: '/get-modal',
//         method: 'POST',
//         cache: false,
//         data: {type: type, item: item},
//         beforeSend: function () {
//             inProgress = true;
//         }
//     }).done(function (data) {
//         console.log(data);
//         $("#modal-style-body").html(data.data);
//         $('#layoutsModal').modal('show');
//         inProgress = false;
//     });
// });

$(".review-video-link").modalVideo();
$(".blog-link-video").modalVideo();

var navListItems = $('div.setup-panel div a'),
allWells = $('.setup-content'),
allNextBtn = $('.nextBtn');

allWells.hide();

navListItems.click(function (e) {
    e.preventDefault();
    var $target = $($(this).attr('href')),
    $item = $(this);

    if (!$item.hasClass('disabled')) {
        navListItems.removeClass('btn-primary').addClass('btn-default');
        $item.addClass('btn-primary');
        allWells.hide();
        $target.show();
        $target.find('input:eq(0)').focus();
    }
});

allNextBtn.click(function () {
    var curStep = $(this).closest(".setup-content"),
    curStepBtn = curStep.attr("id"),
    nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
    curInputs = curStep.find("input[type='text'],input[type='url']"),
    isValid = true;

    $(".form-group").removeClass("has-error");
    for (var i = 0; i < curInputs.length; i++) {
        if (!curInputs[i].validity.valid) {
            isValid = false;
            $(curInputs[i]).closest(".form-group").addClass("has-error");
        }
    }

    if (isValid)
        nextStepWizard.removeAttr('disabled').trigger('click');
});

$('div.setup-panel div a.btn-primary').trigger('click');


$('#discountModal').on('show.bs.modal', function (e) {
    navListItems[0].click();
    var type = $(e.relatedTarget).attr("data-type");
    var plan = $(e.relatedTarget).attr("data-plan");
    $('.select').fadeOut();
    $('div[id*="step-type-' + type + '"]').fadeIn();
    $('#discountModal').find("input[name='valueSquare']").val("");
    $('#discountModal').find("input[name='name']").val("");
    $('#discountModal').find("input[name='name']").val("");
    $('#discountModal').find("input[name='phone']").val("");
    $('#discountModal').find("input[name='email']").val("");
    if (type === "4") {
        $('#discountModal').find("#type-discount").val('form-action');
    } else {
        $('#discountModal').find("#type-discount").val('form-price');
        $('#discountModal').find("#value-discount").val(type + "|" + plan);
    }
});

$('#discountModal').find("span").on('click', (function () {
    var value = $(this).attr("data-value");
    $('#discountModal').find("input[type='radio']").removeAttr('checked');
    $('#discountModal').find("#select-" + value).attr('checked', 'checked');
}));

$('#questionModal').on('show.bs.modal', function (e) {
    navListItems[0].click();
    var type = $(e.relatedTarget).attr("data-type");
    if (type === "query") {
        $('#label-query').show();
        $('#text-query').show();
        $('#label-review').hide();
        $('#text-review').hide();
        $('#label-discount').hide();
        $('#questionModal').find("#type-question").val('form-query');
    } else if (type === "review") {
        $('#label-query').hide();
        $('#text-query').hide();
        $('#label-discount').hide();
        $('#label-review').show();
        $('#questionModal').find("#type-question").val('form-review');
    } else if (type === "discount") {
        $('#label-query').hide();
        $('#text-query').hide();
        $('#label-review').hide();
        $('#text-review').hide();
        $('#label-discount').show();
        var value = $(e.relatedTarget).attr("data-value");
        $('#questionModal').find("#type-question").val('form-discount');
        $('#questionModal').find("#value-question").val(value);
    }
});

var $gridBlog = $('#blog-items').isotope({
    itemSelector: '.blog-container',
    hiddenStyle: {
        opacity: 0
    },
    visibleStyle: {
        opacity: 1
    },
    masonry: {
        gutter: 10,
        horizontalOrder: true,
        fitWidth: true
    },
});

$(".nav-blog-category").on('click', function () {
    var value = $(this).attr("data-value");
    if (value === '*') {
        $gridBlog.isotope({filter: '*'});
    } else {
        $gridBlog.isotope({filter: '.blog-category-' + value});
    }
});

$(window).resize(function () {
    var ww = $(window).width();
    if (ww > 575 && ww <= 768) {
        if ($('#blogSlider').length) {
            blogSlider.params.slidesPerView = 2;
            blogSlider.params.spaceBetween = 45;
        }
        if ($('#moreProject').length) {
            moreProject.params.slidesPerView = 2;
        }
        if ($('#videoSlider').length) {
            videoSlider.params.slidesPerView = 2;
            videoSlider.params.centeredSlides = false;
        }
        if ($('#chooseSlider').length) {
            chooseSlider.params.slidesPerView = 2;
            chooseSlider.params.centeredSlides = false;
        }
        if ($('#reviewSlider').length) {
            reviewSlider.params.slidesPerView = 1;
            reviewSlider.params.centeredSlides = true;
        }
        if ($('#workersSlider').length) {
            workersSlider.params.slidesPerView = 2;
            workersSlider.params.centeredSlides = true;
        }
    } else if (ww <= 575) {
        if ($('#blogSlider').length) {
            blogSlider.params.slidesPerView = 1;
            blogSlider.params.slidesOffsetBefore = 10;
            blogSlider.params.spaceBetween = 7;
        }
        if ($('#moreProject').length) {
            moreProject.params.slidesPerView = 1;
            moreProject.params.slidesOffsetBefore = 23;
        }
        if ($('#videoSlider').length) {
            videoSlider.params.slidesPerView = 1;
            videoSlider.params.centeredSlides = true;
        }
        if ($('#chooseSlider').length) {
            chooseSlider.params.slidesPerView = 1;
            chooseSlider.params.centeredSlides = true;
        }
        if ($('#reviewSlider').length) {
            reviewSlider.params.slidesPerView = 1;
            reviewSlider.params.centeredSlides = true;
        }
        if ($('#workersSlider').length) {
            workersSlider.params.slidesPerView = 1;
            workersSlider.params.centeredSlides = true;
        }
    } else if (ww >= 768 && ww <= 1024) {
        if ($('#workersSlider').length) {
            workersSlider.params.slidesPerView = 3;
            workersSlider.params.centeredSlides = false;
        }
    } else {
        if ($('#blogSlider').length) {
            blogSlider.params.slidesPerView = 3;
            blogSlider.params.centeredSlides = false;
        }
        if ($('#moreProject').length) {
            moreProject.params.slidesPerView = 3;
            moreProject.params.centeredSlides = false;
        }
        if ($('#videoSlider').length) {
            videoSlider.params.slidesPerView = 3;
            videoSlider.params.centeredSlides = false;
        }
        if ($('#chooseSlider').length) {
            chooseSlider.params.slidesPerView = 3;
            chooseSlider.params.centeredSlides = false;
        }
        if ($('#reviewSlider').length) {
            reviewSlider.params.slidesPerView = 2;
            reviewSlider.params.centeredSlides = false;
        }
        if ($('#workersSlider').length) {
            workersSlider.params.slidesPerView = 4;
            workersSlider.params.centeredSlides = false;
        }
    }
    if ($('#blogSlider').length) {
        blogSlider.update();
    }
    if ($('#moreProject').length) {
        moreProject.update();
    }
    if ($('#videoSlider').length) {
        videoSlider.update();
    }
    if ($('#chooseSlider').length) {
        chooseSlider.update();
    }
    if ($('#reviewSlider').length) {
        reviewSlider.update();
    }
    if ($('#workersSlider').length) {
        workersSlider.update();
    }
});
$(window).trigger('resize');

$('.partner-item').hover(function () {
    var img = $(this).find('img');
    var oldImg = img.attr('src');
    var newImg = img.attr('data-content');
    img.attr('src', newImg);
    img.attr('data-content', oldImg);
}, function () {
    var img = $(this).find('img');
    var oldImg = img.attr('src');
    var newImg = img.attr('data-content');
    img.attr('src', newImg);
    img.attr('data-content', oldImg);
}
);

$('.checklist-form').on('submit', function (e) {
    window.open($('#download').prop('href'), '_blank');
});
});

(function (d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s);
    js.id = id;
    js.src = "https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.0";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

$(document).ready(function () {
    $('.fb-share').click(function (e) {
        e.preventDefault();
        window.open($(this).attr('href'), 'fbShareWindow', 'height=450, width=550, top=' + ($(window).height() / 2 - 275) + ', left=' + ($(window).width() / 2 - 225) + ', toolbar=0, location=0, menubar=0, directories=0, scrollbars=0');
        return false;
    });
});