<?php
/**
 * Created by PhpStorm.
 * User: Minor946
 * Date: 11/12/2018
 * Time: 12:29 PM
 */
?>
<footer id="contacts">
    <div class="container-fluid">
        <div class="row">
            <div class="col mx-auto">
                <img src="{{url("/images/footer-logo-v3.svg")}}" alt="footer logo">
            </div>
            <div class="col mx-auto">
                <ul class="footer-list footer-links">
                    <li><a href="#whychooseus">{{ __('navbar.about_company') }}</a></li>
                    <li><a href="#service">{{ __('navbar.service') }}</a></li>
                    <li><a href="#portfolio">{{ __('navbar.portfolio') }}</a></li>
                    <li><a href="#price">{{ __('navbar.price') }}</a></li>
                    <li><a href="{{url('/partners')}}">{{ __('navbar.partners') }}</a></li>
                    <li><a href="{{url('/blogs')}}">{{ __('navbar.blogs') }}</a></li>
                    <li><a href="{{url('/actions')}}">{{ __('navbar.discount') }}</a></li>
                    <li><a href="#contacts">{{ __('navbar.contacts') }}</a></li>
                </ul>
                {{--<ul class="footer-list footer-list2 ">--}}
                    {{--<li><a href="{{url('/policy')}}">{{ __('navbar.policy') }}</a></li>--}}
                    {{--<li><a href="{{url('/agreement')}}">{{ __('navbar.agreement') }}</a></li>--}}
                {{--</ul>--}}
            </div>
            <div class="col mx-auto">
                <label>{{__('footer.footer.phone')}}</label>
                <ul class="footer-list">
                    <li><a href="tel:{{ config('footer_phone_1')}}" target="_blank">{{config('footer_phone_1')}}</a>
                    </li>
                    <li><a href="tel:{{ config('footer_phone_2')}}" target="_blank">{{config('footer_phone_2')}}</a>
                    </li>
                    <li><a href="tel:{{ config('footer_phone_3')}}" target="_blank">{{config('footer_phone_3')}}</a>
                    </li>
                    <li><a href="tel:{{ config('footer_phone_4')}}" target="_blank">{{config('footer_phone_4')}}</a>
                    </li>
                </ul>

                <label>{{__('footer.footer.whatapp')}}</label>
                <ul class="footer-list">
                    <li>
                        <a href="https://api.whatsapp.com/send?phone=<?=str_replace(['+', ' ', '(', ')', '-'], '', config('whatsapp_chanel'));?>"
                           target="_blank">{{config('whatapp')}}</a></li>
                </ul>
                <label>{{__('footer.footer.email')}}</label>
                <ul>
                    <li>{{__('footer.footer.office')}} <a href="mailto:{{config('office_email')}}" target="_blank">{{config('office_email')}}</a></li>
                    <li>{{__('footer.footer.zakup')}} <a href="mailto:{{config('zakup_email')}}" target="_blank">{{config('zakup_email')}}</a></li>
                    <li>{{__('footer.footer.support')}} <a href="mailto:{{config('support_email')}}" target="_blank">{{config('support_email')}}</a></li>
                </ul>
            </div>
            <div class="col mx-auto">
                <label>{{__('footer.footer.office')}}</label>
                <p>
                    {{__('footer.footer.address_town')}}<br>
                    {{__('footer.footer.address')}}
                </p>
                <label>{{__('footer.footer.social')}}</label>
                <ul class="list-inline">
                    <li><a href="{{config('youtube_chanel')}}" target="_blank"><i
                                    class="fa fa-footer fa-youtube-play"></i></a></li>
                    <li><a href="{{config('facebook_chanel')}}" target="_blank"><i
                                    class="fa fa-footer fa-facebook-square"></i></a></li>
                    <li><a href="{{config('instagram_chanel')}}" target="_blank"><i
                                    class="fa fa-footer fa-instagram"></i></a></li>
                    <li><a href="{{config('pinterest_chanel')}}" target="_blank"><i
                                    class="fa fa-footer fa-pinterest"></i></a></li>
                </ul>

                <label>{{__('footer.footer.payments')}}</label>
                <ul class="list-inline">
                    <li>
                        <a href="javascript:void(0);"><i class="fa fa-footer fa-cc-visa"></i></a>
                    </li>
                    <li>
                        <a href="javascript:void(0);"><i class="fa fa-footer fa-cc-mastercard"></i></a>
                    </li>
                    <li>
                        <a href="javascript:void(0);"><i class="fa fa-footer fa-bitcoin"></i></a>
                    </li>
                    <li>
                        <a href="javascript:void(0);">
                            <div  class="elsom" src="{{url('/images/elsom.svg')}}"></div>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="col mx-auto">
                <div class="footer-act" align="center">
                    <label>{{__('footer.footer.subscribe')}}</label>
                    <br>
                    <button class="btn btn-primary btn-main bnt-project btn-all" data-toggle="modal"
                            data-target="#subscribe">{{ __('navbar.subscribe') }}</button>
                </div>
                <br>
                <div align="center">
                    <label>{{__('footer.footer.otk')}}</label>
                    <br>
                    <button class="btn btn-primary btn-main bnt-project btn-all" data-toggle="modal"
                            data-target="#questionModal" data-type="review">{{ __('footer.make_query') }}</button>
                </div>
            </div>
        </div>
    </div>
    <div id="scrollup"></div>
</footer>
