<?php
/**
 * Created by PhpStorm.
 * User: Minor946
 * Date: 11/12/2018
 * Time: 12:29 PM
 */

use Illuminate\Support\Facades\Route;
$route = "";

if (!empty(Route::getFacadeRoot()->current())) {
    $route = Route::getFacadeRoot()->current()->uri();
}
if ($route == "/") {
    $route = "";
} else {
    $route = "/";
}
?>
@desktop
<nav class="navbar navbar-expand-lg navbar-light first-navbar">
    <div class="collapse navbar-collapse" id="#navbar-menu">
        <div class="container-fluid" id="navbar-first">
            <div class="row">
                <div class="col-6 mx-auto">
                    <a class="navbar-brand" href="{{url("/")}}">
                        <img src="{{url('/images/logo-min-v3.svg')}}" alt="logo">
                        <div>
                            <p>
                                {{ __('navbar.design') }}<br>
                                {{ __('navbar.arch') }}<br>
                                {{ __('navbar.repair') }}
                            </p>
                        </div>
                    </a>
                </div>
                <div class="col mx-auto">
                    <button class="btn navbar-btn" data-toggle="modal"
                            data-target="#questionModal" data-type="query">{{ __('navbar.make_query') }}</button>
                </div>
                <div class="col navbar-phone">
                    <p><a href="tel:{{ config('navbar_phone_1')}}"
                          target="_blank">{{config('navbar_phone_1')}}</a></p>
                    <p><a href="tel:{{ config('navbar_phone_2')}}"
                          target="_blank">{{config('navbar_phone_2')}}</a></p>
                </div>
                <div class="col mx-auto margin-right-60" align="center">
                    <div class="form-inline mr-auto">
                        <ul class="navbar-nav navbar-lang mr-auto">
                            @foreach(LaravelLocalization::getLocalesOrder() as $localeCode => $properties)
                                <li class="nav-item @if((LaravelLocalization::getCurrentLocale()  == $localeCode) ) active @endif">
                                    <a class="nav-link"  rel="alternate" hreflang="{{ $localeCode }}"
                                       href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}">
                                        {{ $properties['native'] }}
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item">
                            <a class="nav-link"
                               href="https://api.whatsapp.com/send?phone=<?=str_replace(['+', ' ', '(', ')', '-'], '', config('whatsapp_chanel'));?>"
                               target="_blank">
                                <i class="fa fa-whatsapp fa-2x"></i>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="tg://resolve?domain={{config('telegram_chanel')}}" target="_blank">
                                <i class="fa fa-telegram fa-2x"></i>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{config('youtube_chanel')}}" target="_blank">
                                <i class="fa fa-youtube-play fa-2x"></i>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{config('instagram_chanel')}}" target="_blank">
                                <i class="fa fa-instagram fa-2x"></i>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{config('facebook_chanel')}}" target="_blank">
                                <i class="fa fa-facebook-official fa-2x"></i>
                            </a>
                        </li>

                    </ul>
                </div>
            </div>
        </div>
        <div class="container-fluid second-navbar" id="navbar-second">
            <ul class="navbar-nav mr-auto dropdown navbar-center">
                <li class="drop">
                    <a class="nav-link" href="{{$route}}#whychooseus">{{ __('navbar.about_company') }}</a>
                    <ul class="sub_menu">
                        <li><a href="{{url("/jobs")}}"> {{ __('navbar.jobs') }}</a></li>
                        <li><a href="{{$route}}#workers"> {{ __('navbar.workers') }}</a></li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{$route}}#service">{{ __('navbar.service') }}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{$route}}#portfolio">{{ __('navbar.portfolio') }}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{$route}}#price">{{ __('navbar.price') }}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{url('/partners')}}">{{ __('navbar.partners') }}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{url('/blogs')}}">{{ __('navbar.blogs') }}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{url('/actions')}}">{{ __('navbar.discount') }}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#contacts">{{ __('navbar.contacts') }}</a>
                </li>
            </ul>
        </div>
    </div>
</nav>
@elsedesktop
<div class="navbar-mobile">
    <nav class="navbar navbar-expand-xl fixed-top navbar-light bg-light navbar-mobile">
        <div class="navbar-body">
            <button class="hamburger hamburger--slider" type="button" data-toggle="collapse"
                    data-target="#navbarNavAltMarkup"
                    aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
              <span class="hamburger-box">
                <span class="hamburger-inner"></span>
              </span>
            </button>
            <a class="navbar-brand" href="{{url("/")}}">
                <img src="{{url('/images/logo-min-v3.svg')}}" alt="logo">
            </a>

            <button class="btn navbar-btn" data-toggle="modal"
                    data-target="#questionModal" data-type="query">{{ __('navbar.make_query') }}</button>
        </div>

        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav light">
                <a class="nav-link" href="{{$route}}#whychooseus">{{ __('navbar.about_company') }}</a>
                <a class="nav-link" href="{{url("/jobs")}}"> {{ __('navbar.jobs') }}</a>
                <a class="nav-link" href="{{$route}}#service">{{ __('navbar.service') }}</a>
                <a class="nav-link" href="{{$route}}#portfolio">{{ __('navbar.portfolio') }}</a>
                <a class="nav-link" href="{{$route}}#price">{{ __('navbar.price') }}</a>
                <a class="nav-link" href="{{url('/partners')}}">{{ __('navbar.partners') }}</a>
                <a class="nav-link" href="{{url('/blogs')}}">{{ __('navbar.blogs') }}</a>
                <a class="nav-link" href="{{url('/actions')}}">{{ __('navbar.discount') }}</a>
                <a class="nav-link" href="#contacts">{{ __('navbar.contacts') }}</a>
            </div>
            <div class="navbar-nav dark">
                <label class="label-navbar"><i class="fa fa-phone"></i>Позвонить на телефон: </label>

                <a href="tel:{{ config('navbar_phone_1')}}"
                   target="_blank">{{config('navbar_phone_1')}}</a>
                <a href="tel:{{ config('navbar_phone_2')}}"
                   target="_blank">{{config('navbar_phone_2')}}</a>

                <label class="label-navbar-2"><i class="fa fa-whatsapp"></i>Позвонить на WhatsApp: </label>

                <a href="https://api.whatsapp.com/send?phone=<?=str_replace(['+', ' ', '(', ')', '-'], '', config('whatsapp_chanel'));?>"
                   target="_blank">
                    {{config('whatsapp_chanel')}}
                </a>

                <div class=" navbar-social">
                    <a class="nav-link" href="tg://resolve?domain={{config('telegram_chanel')}}" target="_blank">
                        <i class="fa fa-telegram fa-2x"></i>
                    </a>
                    <a class="nav-link" href="{{config('youtube_chanel')}}" target="_blank">
                        <i class="fa fa-youtube-play fa-2x"></i>
                    </a>
                    <a class="nav-link" href="{{config('instagram_chanel')}}" target="_blank">
                        <i class="fa fa-instagram fa-2x"></i>
                    </a>
                    <a class="nav-link" href="{{config('facebook_chanel')}}" target="_blank">
                        <i class="fa fa-facebook-official fa-2x"></i>
                    </a>
                    @foreach(LaravelLocalization::getLocalesOrder() as $localeCode => $properties)
                            <a class="nav-link"  rel="alternate" hreflang="{{ $localeCode }}"
                               href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}">
                                {{ $properties['native'] }}
                            </a>
                    @endforeach

                </div>
            </div>
        </div>
    </nav>
</div>
@enddesktop
