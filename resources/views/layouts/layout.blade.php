<?php
/**
 * Created by PhpStorm.
 * User: Minor946
 * Date: 11/12/2018
 * Time: 12:35 PM
 */
?>
        <!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>NEOSTYLE ARCHITECTURAL GROUP | Дизайн интерьера в Бишкеке | Элитный дизайн интерьера | Архитектурное
        проектирование | Ремонт и отделка | Ландшафтный дизайн - @yield('title')</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Fonts -->
    @yield('meta')
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link rel="icon" type="image/png" href="{{{ url('/images/fabicon.png') }}}">

    <link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/responsive.css') }}" rel="stylesheet" type="text/css">
    <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-34909143-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-34909143-1');
</script>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-NKVPLZ');</script>
<!-- End Google Tag Manager -->

<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
   (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
   m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
   (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

   ym(26307888, "init", {
        id:26307888,
        clickmap:false,
        trackLinks:false,
        accurateTrackBounce:false,
        webvisor:false
   });
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/26307888" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->

</head>
<body>
    <!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NKVPLZ"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<div id="app">
    @section('navbar')
        @include('layouts.navbar')
    @show

    <div class="wrapper">
        @yield('content')
    </div>

    @section('footer')
        @include('layouts.footer')
    @show
</div>

@include('modals.modal_question')
@include('modals.modal_check_list')
@include('modals.modal_discount')
@include('modals.modal_by_first_load')
@include('modals.modal_subscribe')

<script src="{{ asset('js/app.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/swiper.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/isotope.pkgd.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/js.cookie.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/lazyload.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/scripts.js') }}" type="text/javascript"></script>
<script type="text/javascript" data-pin-custom="true" async defer src="//assets.pinterest.com/js/pinit.js"></script>
<div id="fb-root"></div>
@yield('scripts')
<!-- BEGIN JIVOSITE CODE {literal} -->
<script type="text/javascript">
  (function (w, d) {
    w.amo_jivosite_id = 'w125BlMnci';
    var s = document.createElement('script'), f = d.getElementsByTagName('script')[0];
    s.id = 'amo_jivosite_js';
    s.type = 'text/javascript';
    s.async = true;
    s.src = 'https://forms.amocrm.ru/chats/jivosite/jivosite.js';
    f.parentNode.insertBefore(s, f);
  })(window, document);
</script>
<!-- {/literal} END JIVOSITE CODE -->

</body>
</html>