<?php
/**
 * Created by PhpStorm.
 * User: Minor946
 * Date: 11/12/2018
 * Time: 12:35 PM
 */
?>
@extends('layouts.layout')

@section('title', __('title.BlogsPage'))

@section('meta')
    @parent
    <meta name="description" lang="ru" content="{{__('meta.BlogsPageDescription')}}">
    <meta name="keywords" lang="ru" content="{{__('meta.BlogsPageKeywords')}}">
@endsection


@section('content')
    @include('components.blogs.list')
@endsection


