<?php
/**
 * Created by PhpStorm.
 * User: Minor946
 * Date: 11/12/2018
 * Time: 12:35 PM
 */

/** @var \App\Models\PortfolioModel $model */

?>
@extends('layouts.layout')

@section('title', $model->name)

@section('meta')
    @parent
    <meta name="keywords" lang="ru"
          content="{{ $model->name }},{{ $model->categoryStyle->name }}, {{__('meta.ProjectKeywords')}}, ">

    <meta property="og:url" content="{{url($_SERVER['REQUEST_URI']) }}"/>
    <meta property="og:type" content="website"/>
    <meta property="og:title" content="{{$model->name}}"/>
    <meta property="og:description" content="{{$model->description}}"/>
    <meta property="og:image" content="{{url("/storage/" . $model->image)}}"/>
@endsection



@section('content')
    @include('components.portfolio.view',['model'=>$model])
@endsection

