<?php
/**
 * Created by PhpStorm.
 * User: lolbu
 * Date: 25.11.2018
 * Time: 22:20
 */

/** @var \App\Models\PortfolioModel $model */
use Intervention\Image\ImageManager;

$manager = new ImageManager(array('driver' => 'gd'));

?>

<section class="section-white padding-top-20">
    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url("/")}}">{{__('title.MainPage')}}</a></li>
                <li class="breadcrumb-item"><a href="{{url("/portfolio")}}">{{__('navbar.portfolio')}}</a></li>
                <li class="breadcrumb-item active" aria-current="page">{{$model->name}}</li>
            </ol>
        </nav>

        <div class="label-portfolio portfolio-data">
            <h2>{{$model->name}}</h2>
        </div>
        <div class="portfolio-body">
            <div class="row">
                <div class="col-12 portfolio-data">
                    <p>
                        <strong>{{__('main.portfolio.card.style')}}</strong> <?=$model->categoryStyle->name?><br>
                        <strong>{{__('main.portfolio.card.square')}}</strong> <?=$model->square?><br>
                        <strong>{{__('main.portfolio.card.location')}}</strong> <?=$model->location?><br>
                    </p>
                </div>
                <div class="col-12 portfolio-desc">
                    <div class="portfolio-body-desc">
                        @if(isset($model->description))
                            {!!$model->description!!}
                        @endif
                    </div>
                </div>
            </div>
            <div class="portfolio-slider">
                <?php if(!empty($model->panorama)):?>
                <div id="panorama"></div>
                <?php endif;?>
                @if(config('protfolio_type') == 'gallery')
                    <div class="lightgallery" align="center">
                        <a href="{{url("/storage/" . $model->image)}}">
                            <img class="lazy gallery_img" src="{{url("/storage/" . $model->image)}}"/>
                        </a>
                        <?php if(!empty($model->sliders)):?>
                        <?php foreach($model->sliders as $slider):?>
                        <a href="{{url("/storage/" . $slider)}}">
                            <img class="lazy gallery_img" src="{{url("/storage/" . $slider)}}"/>
                        </a>
                        <?php endforeach;?>
                        <?php endif;?>
                    </div>
                @else
                    <div class="swiper-container swiper-portfolio" id="projectSlider">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <?php
                                $image = (string)$manager->make("storage/" . $model->image)->resize(900, 600)->encode('data-url');
                                ?>
                                <img src="<?=$image?>">
                            </div>
                            <?php if(!empty($model->sliders)):?>
                            <?php foreach($model->sliders as $slider):?>
                            <div class="swiper-slide">
                                <?php
                                $image = (string)$manager->make("storage/" . $slider)->resize(900, 600)->encode('data-url');
                                ?>
                                <img src="<?=$image?>">
                            </div>
                            <?php endforeach;?>
                            <?php endif;?>
                        </div>
                        <div class="swiper-pagination choose-pagination portfolio-pagination"></div>
                        <div class="arrow-left portfolio-button-prev"></div>
                        <div class="arrow-right portfolio-button-next"></div>
                    </div>
                @endif


            </div>
            <div class="clearfix"></div>
            {{--Other rooms--}}
            <?php if(!empty($model->rooms)):?>
            <div class="portfolio-rooms" align="center">
                <?php foreach ($model->rooms as $room):?>
                <div class="row">
                    <div class="col-12">
                        <strong>
                            <p class="room-label"><?= $room->name ?></p>
                        </strong>
                        <hr>
                        <div class="clearfix"></div>
                        <div class="portfolio-body-desc">
                            @if(isset($room->description))
                                {!!$room->description!!}
                            @endif
                        </div>
                    </div>
                    <?php if(!empty($room->images)):?>
                    <div class="col-12">
                        @if(config('protfolio_type') == 'gallery')
                            <div class="lightgallery">
                                <?php foreach($room->images as $image):?>
                                <a href="{{url("storage/" . $image)}}">
                                    <img class="lazy gallery_img" src="{{url("storage/" . $image)}}"/>
                                </a>
                                <?php endforeach;?>
                            </div>
                        @else
                            <div class="portfolio-slider">

                                <div class="swiper-container swiper-portfolio" id="roomSlider<?=$room->id?>">
                                    <div class="swiper-wrapper">
                                        <?php foreach($room->images as $image):?>
                                        <div class="swiper-slide">
                                            <?php
                                            $image = (string)$manager->make("storage/" . $image)->resize(900, 600)->encode('data-url');
                                            ?>
                                            <img src="<?=$image?>">
                                        </div>
                                        <?php endforeach;?>
                                    </div>
                                    <div class="swiper-pagination choose-pagination portfolio-pagination portfolio-pagination<?=$room->id?>"></div>
                                    <div class="arrow-left portfolio-button-prev portfolio-button-prev<?=$room->id?>"></div>
                                    <div class="arrow-right portfolio-button-next portfolio-button-next<?=$room->id?>"></div>
                                </div>
                            </div>
                        @endif
                    </div>

                    @section('scripts')
                        @parent
                        <script type="text/javascript">
                            var blogSlider = new Swiper('#roomSlider<?=$room->id?>', {
                                loop: false,
                                autoplay: 600,
                                spaceBetween: 0,
                                navigation: {
                                    nextEl: '.portfolio-button-next<?=$room->id?>',
                                    prevEl: '.portfolio-button-prev<?=$room->id?>',
                                },
                                pagination: {
                                    el: '.portfolio-pagination<?=$room->id?>',
                                    clickable: true,
                                },
                            });
                        </script>
                    @endsection
                    <?php endif;?>
                </div>
                <?php endforeach;?>
            </div>
            <?php endif;?>
        </div>

        <div class="container" align="center">
            <a class="btn btn-primary btn-main"
               href="{{url("/".\App\Models\PortfolioModel::find($model->getNext())->alias)}}">{{__('main.portfolio.nextProject')}}</a>
        </div>

        <div class="container blog-container padding-top-50">
            <?php
            $query = "";
            foreach ($model->categories as  $key => $category) {
                if($key == count($model->categories) - 1 ){
                    $query .= " (find_in_set('".$category."', categories))";
                }else{
                    $query .= " (find_in_set('".$category."', categories)) OR";
                }
            }
            $projects = \App\Models\PortfolioModel::whereRaw('( category_style = ' . $model->category_style . ' OR '.$query.') AND status = 1 AND id != ' . $model->id)->groupBy('id')->orderBy(DB::raw('RAND()'))->take(6)->get();
            ?>
            <?php if(count($projects) > 0):?>
            <div class="label-portfolio" align="center">
                <h2>{{__('portfolio.more_projects')}}</h2>
            </div>
            <div class="swiper-container" id="moreProject">
                <div class="swiper-wrapper">

                    <?php foreach ($projects as $key => $project):?>
                    <div class="swiper-slide">
                        @include('components.items.portfolio_item', ['project'=>$project])
                    </div>
                    <?php endforeach;?>
                </div>
            </div>
            <?php endif;?>

        </div>
    </div>
</section>
@section('scripts')
    @parent
    <script type="text/javascript">
        $(document).ready(function () {
            $(".lightgallery").lightGallery({thumbnail: true});
        });
        <?php if(!empty($model->panorama)):?>
        pannellum.viewer('panorama', {
            "type": "equirectangular",
            "panorama": '{{url("/storage/".$model->panorama)}}',
            "autoLoad": true
        });
        <?php endif;?>
    </script>
@endsection
<style>
    @media all and (max-width: 575px) {
        .swiper-wrapper {
            margin-left: -25px;
        }
    }
</style>