<?php
/**
 * Created by PhpStorm.
 * User: Minor946
 * Date: 11/27/2018
 * Time: 2:21 PM
 */

$categories = \App\Models\CategoriesModel::where('status', \App\Models\ChooseModel::STATUS_SHOW)->where('parent_id', null)->where('type', 0)->orderBy('position', 'asc')->get();
$portfolio = \App\Models\PortfolioModel::where('status', \App\Models\ChooseModel::STATUS_SHOW)->orderBy('position', 'asc')->get();

$parent = app('request')->input('parent');
$sub_category = app('request')->input('sub_category');
?>

<section class="section-white padding-top-20">
    <div class="container portfolio-container" align="center">
        <div class="nav nav-pills portfolio-tab" id="nav-portfolio" role="tablist">
            <a class="nav-item nav-link <?= (empty($parent)) ? 'active' : ''?> nav-portfolio-category" id="nav-home-tab"
               data-toggle="tab"
               href="javascript:void(0);"
               role="tab"
               aria-controls="nav-home" data-value="*"
               aria-selected="<?= (empty($parent)) ? 'true' : 'false'?>">{{__('main.portfolio.showAll')}}</a>

            @foreach ($categories as $category)
                <a class="nav-item nav-link nav-portfolio-category <?= (!empty($parent) && $category->id == $parent) ? 'active' : ''?>"
                   data-toggle="tab" href="javascript:void(0);"
                   role="tab" aria-selected="<?= (!empty($parent) && $category->id == $parent) ? 'true' : 'false'?>"
                   data-value="{{$category->id}}">{{$category->name}}</a>
            @endforeach

        </div>
        <hr class="hr-portfolio">
        <div class="portfolio-sub-category"></div>
        <div class="tab-content" align="left">
            <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                <div class="portfolio-sub-categories" align="center">
                    <?php if (!empty($parent)): ?>
                <?php
                        $categoriesMain = \App\Models\CategoriesModel::whereRaw('FIND_IN_SET(' . $parent . ' , parent_id) AND type = 0 ')
                            ->orderBy('position', 'asc')->get();

                        if (count($categoriesMain) > 0) {
                            $out = "";
                            foreach ($categoriesMain as $categoryMain) {
                                echo view('components.items.category_item', ['category' => $categoryMain, 'parent_id' => $parent, 'sub_category'=>$sub_category]);
                            }
                            echo view('components.items.category_item', ['category' => null, 'parent_id' => $parent]);
                        }
                        ?>
            <?php endif;?>
                </div>
                <div class="portfolio-items" id="portfolio-project">
                    <?php foreach($portfolio as $project):?>
                    @include('components.items.portfolio_item', ['project'=>$project])
                    <?php endforeach;?>

                </div>
            </div>

        </div>
    </div>
</section>

@section('scripts')
    @parent
    <script>
        gridBlog();
    </script>
@endsection
