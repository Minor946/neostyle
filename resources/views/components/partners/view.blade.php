<?php
/**
 * Created by PhpStorm.
 * User: Minor946
 * Date: 12/11/2018
 * Time: 2:21 PM
 */
?>

<section class="section-white padding-section" id="partners">
    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url("/")}}">{{__('title.MainPage')}}</a></li>
                <li class="breadcrumb-item active" aria-current="page">{{__('navbar.partners')}}</li>
            </ol>
        </nav>
    </div>
    <div class="label-main" align="center">
        <h2>{{__('main.partners.partnersLabel')}}</h2>
    </div>

    <img class="side-img side-img-10" src="{{url("/images/side8.svg")}}">
    <img class="side-img side-img-11" src="{{url("/images/side1.svg")}}">

    <div class="container container-partners">
        <div class="padding-top-50" align="center">
            <div class="row">
                <div class="col-12 col-md-6">
                    <img class="partners-img" src="{{url("/images/partners.png")}}">
                </div>
                <div class="col-12 col-md-6">
                    <p class="partner-text" style="padding: 0 25px;">
                        {{__("partners.TextFor3Row")}}
                    </p>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="container partner-container">
        <?php
        $partners = \App\Models\PartnersModel::where('status', \App\Models\ChooseModel::STATUS_SHOW)->orderBy('position', 'asc')->take(8)->get();
        ?>
        <?php foreach ($partners as $key => $partner):?>
        @include('components.items.partner_company', ['partner'=>$partner, 'count'=>$key])
        <?php endforeach;?>
    </div>
</section>

@include('modals.modal_layouts')