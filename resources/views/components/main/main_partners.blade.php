<?php
/**
 * Created by PhpStorm.
 * User: lolbu
 * Date: 15.11.2018
 * Time: 1:00
 */
?>
<section id="partners">
    <div class="label-main padding-section" align="center">
        <h2>{{__('main.partners.partnersLabel')}}</h2>
    </div>
    <img class="side-img side-img-5" src="{{url("/images/side5.svg")}}">
    <img class="side-img side-img-6" src="{{url("/images/side6.svg")}}">
    <div class="container partner-container">
        <?php
        $partners = \App\Models\PartnersModel::where('status', \App\Models\ChooseModel::STATUS_SHOW)->orderBy('position', 'asc')->take(8)->get();
        ?>
        <?php foreach ($partners as $key => $partner):?>
        @include('components.items.partner_company', ['partner'=>$partner, 'count'=>$key])
        <?php endforeach;?>
    </div>
</section>


