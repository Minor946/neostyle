<?php
/**
 * Created by PhpStorm.
 * User: Minor946
 * Date: 11/12/2018
 * Time: 3:05 PM
 */
?>

<section id="question">
    <div class="label-main" align="center">
        <h2>{{__('main.question.remained')}}</h2>
        <h2>{{__('main.question.fill_form')}}</h2>
    </div>
    <div class="question-container-form">
        <div class="question-line"></div>
        <img src="{{url("/images/last-callback.png")}}">
        <div class="question-block" align="center">
            {!! Form::open(['action' => 'MainController@callback','id'=>'main-question-form']) !!}
            <div class="form-group">
                <input name="name" type="text" class="form-control" id="exampleInputName1" aria-describedby="emailHelp"
                       required
                       placeholder="{{__('form.placeholder.name')}}">
            </div>
            <div class="form-group">
                <input name="phone" type="text" class="form-control" id="exampleInputPhone1"
                       required
                       placeholder="{{__('form.placeholder.phone')}}">
            </div>
            <div class="form-group">
                <input name="email" type="email" class="form-control" id="exampleInputEmail1"
                       aria-describedby="emailHelp"
                       placeholder="{{__('form.placeholder.email')}}">
            </div>
            {!! Form::hidden('type', 'main-question-form') !!}
            <button type="submit" class="btn btn-primary btn-main">{{__('form.placeholder.send')}}</button>
            {!! Form::close() !!}
        </div>
    </div>
</section>