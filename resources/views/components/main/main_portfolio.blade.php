<?php
/**
 * Created by PhpStorm.
 * User: lolbu
 * Date: 15.11.2018
 * Time: 1:02
 */

use Illuminate\Support\Facades\DB;

$categories = \App\Models\CategoriesModel::where('status', \App\Models\ChooseModel::STATUS_SHOW)->where('parent_id', null)->where('type', 0)->orderBy('position', 'asc')->get();
$portfolio = \App\Models\PortfolioModel::where('status', \App\Models\ChooseModel::STATUS_SHOW)->orderBy(DB::raw('RAND()'))->take(6)->get();

$count = \App\Models\PortfolioModel::where('status', \App\Models\ChooseModel::STATUS_SHOW)->count();

$countRender = 0;
?>

<section class="section-white padding-bottom-50" id="portfolio">
    <div class="label-main padding-top-50" align="center">
        <h2>{{__('main.portfolio.portfolioLabel')}}</h2>
    </div>
    <div class="container portfolio-container" align="center">
        <div class="nav nav-pills portfolio-tab" id="nav-portfolio" role="tablist">
            <a class="nav-item nav-link active nav-portfolio-category" id="nav-home-tab" data-toggle="tab"
               href="javascript:void(0);"
               role="tab"
               aria-controls="nav-home" data-value="*" aria-selected="true">{{__('main.portfolio.showAll')}}</a>
            @foreach ($categories as $category)
                <a class="nav-item nav-link nav-portfolio-category" data-toggle="tab" href="javascript:void(0);"
                   role="tab" aria-selected="false"
                   data-value="{{$category->id}}">{{$category->name}}</a>
            @endforeach
        </div>
        <hr class="hr-portfolio">
        <div class="portfolio-sub-category"></div>
        <div class="tab-content" align="left">
            <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                <div class="portfolio-sub-categories" align="center">
                </div>
                <div class="portfolio-items" id="portfolio-project">
                    <?php foreach($portfolio as $project):?>
                    @include('components.items.portfolio_item', ['project'=>$project, 'countRender' => $countRender])
                    <?php $countRender++ ?>
                    <?php endforeach;?>
                </div>
            </div>
        </div>
        <div class="padding-top-50" align="center">
            <a class="btn btn-primary btn-main bnt-project btn-all" id="btn-all-portfolio"
               href="{{url('/portfolio')}}">{{ __('main.portfolio.showAll') }}</a>
        </div>
    </div>
</section>

@section('scripts')
    @parent
    <script>
        gridMain()
    </script>
@endsection
