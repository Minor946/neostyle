<?php
/**
 * Created by PhpStorm.
 * User: lolbu
 * Date: 15.11.2018
 * Time: 1:01
 */
?>
<section id="price">
    <div class="label-main padding-section" align="center">
        <h2>{{__('main.price.priceLabel')}}</h2>
    </div>

    <div class="container portfolio-container" align="center">
        <div class="nav nav-fill nav-pills price-tab" id="nav-price" role="tablist">
            <a class="nav-item nav-link nav-price active" id="nav-design-tab" data-toggle="tab" href="#design-tab"
               role="tab"
               aria-controls="nav-design" aria-selected="true" data-type="0">{{__('main.price.type_design')}}</a>

            <a class="nav-item nav-price nav-link" id="nav-repair-tab" data-toggle="tab" href="#design-tab"
               role="tab"
               aria-controls="nav-repair" aria-selected="false" data-type="1">{{__('main.price.type_repair')}}</a>

            <a class="nav-item nav-price nav-link" id="nav-arch-tab" data-toggle="tab" href="#design-tab"
               role="tab"
               aria-controls="nav-arch" aria-selected="false" data-type="2">{{__('main.price.type_arch')}}</a>

            <a class="nav-item nav-price nav-link" id="nav-land-tab" data-toggle="tab" href="#design-tab"
               role="tab"
               aria-controls="nav-land" aria-selected="false" data-type="3">{{__('main.price.type_land')}}</a>
        </div>

        <div class="tab-price-content" align="left">

            <h2>{{__('main.price.tariffs')}}</h2>
            <div class="price-body" id="price-body">
                <div class="tab-pane fade show active" id="design-tab" role="tabpanel" aria-labelledby="nav-design">
                    <div class="row">
                        <div class="col-12 col-md-12 col-lg-8 price-type-list">
                            <div class="price-type price-type-item-0">
                                <div class="price-type-item active" data-plan="0">
                                    <div class="price-img price1" src="{{url("/images/price1.svg")}}"></div>
                                    <h2>{{__('main.price.plan_economy')}}</h2>
                                    <h4>{{__('main.price.package')}}</h4>
                                    <a href="javascript:void(0);" class="price-more" data-type="0"
                                       data-plan="0">{{ __('btn.more') }}</a>
                                    <hr>
                                </div>
                            </div>
                            <div class="price-type price-type-item-1">
                                <div class="price-type-item" data-plan="1">
                                    <div class="price-img price2" src="{{url("/images/price2.svg")}}"></div>
                                    <h2>{{__('main.price.plan_comfort')}}</h2>
                                    <h4>{{__('main.price.package')}}</h4>
                                    <a href="javascript:void(0);" class="price-more" data-type="0"
                                       data-plan="1">{{ __('btn.more') }}</a>
                                    <hr>
                                </div>
                            </div>
                            <div class="price-type price-type-item-2">
                                <div class="price-type-item" data-plan="2">
                                    <div class="price-img price3" src="{{url("/images/price3.svg")}}"></div>
                                    <h2>{{__('main.price.plan_maximum')}}</h2>
                                    <h4>{{__('main.price.package')}}</h4>
                                    <a href="javascript:void(0);" class="price-more" data-type="0"
                                       data-plan="2">{{ __('btn.more') }}</a>
                                    <hr>
                                </div>
                            </div>
                            <div class="price-type price-type-item-3">
                                <div class="price-type-item" data-plan="3">
                                    <div class="price-img price4" src="{{url("/images/price4.svg")}}"></div>
                                    <h2>{{__('main.price.plan_premium')}}</h2>
                                    <h4>{{__('main.price.package')}}</h4>
                                    <a href="javascript:void(0);" class="price-more" data-type="0"
                                       data-plan="3">{{ __('btn.more') }}</a>
                                    <hr>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-12 col-lg-4 price-container">
                            <div class="price-description">
                                <button type="button" class="close" id="price-close" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <ul class="list-styled" id="priceList">
                                    <?php
                                    $price_list = \App\Models\PriceListModel::where('type', \App\Models\PriceListModel::TYPE_DESIGN)
                                        ->where('plan', \App\Models\PriceListModel::PLAN_ECONOMY)->get();
                                    ?>
                                    <?php foreach ($price_list as $item):?>
                                    @include('components.items.price_list_item', ['item'=>$item->name])
                                    <?php endforeach;?>
                                </ul>

                                <a href="javascript:void(0);" class="btn btn-main btn-price" data-toggle="modal"
                                   data-target="#discountModal"
                                   data-type="0" data-plan="0">{{__('main.price.get_cost')}}</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade show active" id="repair-tab" role="tabpanel"
                     aria-labelledby="nav-repair"></div>
                <div class="tab-pane fade show active" id="arch-tab" role="tabpanel" aria-labelledby="nav-arch"></div>
                <div class="tab-pane fade show active" id="land-tab" role="tabpanel" aria-labelledby="nav-land"></div>
            </div>

        </div>
    </div>
</section>
