<?php
/**
 * Created by PhpStorm.
 * User: lolbu
 * Date: 15.11.2018
 * Time: 1:07
 */

$sliders = \App\Models\SlidersModel::where('status', \App\Models\ChooseModel::STATUS_SHOW)->orderBy('position', 'asc')->get();
?>
<section class="section-white">
    <div class="container list-on-slider">
        <ul>
            <li>{{ __('slider.point1') }}</li>
            <li>{{ __('slider.point2') }}</li>
            <div class="list-hr"></div>
            <li>{{ __('slider.point3') }}</li>
            <li>{{ __('slider.point4') }}</li>
        </ul>
    </div>
    <div class="swiper-container" id="mainSlider">
        <div class="swiper-wrapper">
            <?php foreach ($sliders as $slider) :?>
            <div class="swiper-slide main-slide">
                <div class="container">
                    <div class="row">
                        <div class="col-12 col-md-5 main-slide-text">
                            <h2><?=$slider->title_h3?></h2>
                            <h3><?=$slider->title_h2?></h3>
                            <a href="{{url("/actions")}}#action-<?=$slider->id?>"
                               class="btn btn-primary btn-main btn-slider">{{ __('btn.more') }}</a>
                        </div>
                        <div class="col-12 col-md-7">
                            <img class="lazy" src="{{url("/storage/" . $slider->image)}}">
                            <div align="center">
                                <a href="{{url("/actions")}}#action-<?=$slider->id?>"
                                   class="btn btn-primary btn-main btn-slider btn-main2">{{ __('btn.more') }}</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php endforeach;?>
        </div>
        <div class="container slider-control">
            <div class="slider-btn">
                <div class="arrow-left main-button-prev"></div>
                <div class="arrow-right main-button-next"></div>
            </div>
            <div class="main-pagination"></div>
        </div>
    </div>
    <div align="center">
        <a href="#whychooseus">
            <div class="btn-down"></div>
        </a>
    </div>
</section>