<?php
/**
 * Created by PhpStorm.
 * User: lolbu
 * Date: 15.11.2018
 * Time: 1:03
 */
?>
<section class="without-padding" id="service">
    <div class="label-main padding-section" align="center">
        <h2>{{__('main.service.Service')}}</h2>
    </div>
    <img class="side-img side-img-3" src="{{url("/images/side3.svg")}}">
    <img class="side-img side-img-4" src="{{url("/images/side4.svg")}}">
    @desktop
    <div class="container service-container">
        <div class="service-item service-item-0">
            <h4>{{ __('messages.design') }}<br> {{ __('messages.interior') }}</h4>
            <hr>
            <img src="{{url("/images/service-0.png")}}">
            <ul>
                @include('components.items.service_item', ['type'=>\App\Models\ServicesModel::TYPE_DESIGN])
            </ul>
        </div>
        <div class="service-item service-item-1">
            <h4>{{ __('messages.repair_and') }}<br> {{ __('messages.furnish') }}</h4>
            <hr>
            <img src="{{url("/images/service-1.png")}}">
            <ul>
                @include('components.items.service_item', ['type'=>\App\Models\ServicesModel::TYPE_REPAIR])
            </ul>
        </div>

        <div class="service-item service-item-2">
            <h4>{{ __('messages.architect') }}<br> {{ __('messages.projection') }}</h4>
            <hr>
            <img src="{{url("/images/service-2.png")}}">
            <ul>
                @include('components.items.service_item', ['type'=>\App\Models\ServicesModel::TYPE_ARCHITECT])
            </ul>
        </div>

        <div class="service-item service-item-3">
            <h4>{{ __('messages.landscape') }}<br> {{ __('messages.design') }}  </h4>
            <hr>
            <img src="{{url("/images/service-3.png")}}">
            <ul>
                @include('components.items.service_item', ['type'=>\App\Models\ServicesModel::TYPE_LANDSCAPE])
            </ul>
        </div>
    </div>
    @elsedesktop
    <div class="swiper-container" id="serviceSlider">
        <div class="swiper-wrapper">
            <div class="swiper-slide">
                <div class="service-item service-item-0">
                    <h4>{{ __('messages.design') }}<br> {{ __('messages.interior') }}</h4>
                    <hr>
                    <img src="{{url("/images/service-0.png")}}">
                    <ul>
                        @include('components.items.service_item', ['type'=>\App\Models\ServicesModel::TYPE_DESIGN])
                    </ul>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="service-item service-item-1">
                    <h4>{{ __('messages.repair_and') }}<br> {{ __('messages.furnish') }}</h4>
                    <hr>
                    <img src="{{url("/images/service-1.png")}}">
                    <ul>
                        @include('components.items.service_item', ['type'=>\App\Models\ServicesModel::TYPE_REPAIR])
                    </ul>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="service-item service-item-2">
                    <h4>{{ __('messages.architect') }}<br> {{ __('messages.projection') }}</h4>
                    <hr>
                    <img src="{{url("/images/service-2.png")}}">
                    <ul>
                        @include('components.items.service_item', ['type'=>\App\Models\ServicesModel::TYPE_ARCHITECT])
                    </ul>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="service-item service-item-3">
                    <h4>{{ __('messages.landscape') }}<br> {{ __('messages.design') }}  </h4>
                    <hr>
                    <img src="{{url("/images/service-3.png")}}">
                    <ul>
                        @include('components.items.service_item', ['type'=>\App\Models\ServicesModel::TYPE_LANDSCAPE])
                    </ul>
                </div>
            </div>
        </div>
        <div class="swiper-pagination choose-pagination service-pagination"></div>
    </div>
    @section('scripts')
        @parent
        <script type="text/javascript">
            new Swiper('#serviceSlider', {
                loop: false,
                spaceBetween: 0,
                slidesPerView: 1,
                centeredSlides: true,
                pagination: {
                    el: '.service-pagination',
                    clickable: true,
                },
            });
        </script>
    @endsection
    @enddesktop

</section>
