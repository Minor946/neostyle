<?php
/**
 * Created by PhpStorm.
 * User: lolbu
 * Date: 15.11.2018
 * Time: 1:03
 */
?>

<section id="checklist">
    <div class="container checklist-container  padding-section">
        <h2 class="checklist-label">{{__('main.checklist.ChecklistLabel')}}</h2>
        <div class="checklist-body">
            <img src="{{url("/images/check-number.svg")}}">
            <div>
                <h2>{{__('main.checklist.solution')}}</h2>
                <h4>{{__('main.checklist.description')}}</h4>
                <button class="btn btn-main btn-checklist" data-toggle="modal"
                        data-target="#checkListModal">{{__('main.checklist.download')}}</button>
            </div>
            <img src="{{url("/images/check-user.png")}}">
        </div>
    </div>
</section>