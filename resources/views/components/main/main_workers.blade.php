<?php
/**
 * Created by PhpStorm.
 * User: lolbu
 * Date: 15.11.2018
 * Time: 1:00
 */

$workers = \App\Models\WorkersModel::where('status', \App\Models\ChooseModel::STATUS_SHOW)->orderBy('position', 'asc')->get();
?>
<?php if(!empty($workers)):?>
<section id="workers">
    <div class="label-main padding-section" align="center">
        <h2>{{__('main.partners.workersLabel')}}</h2>
    </div>
    <img class="side-img side-img-5" src="{{url("/images/side5.svg")}}">
    <img class="side-img side-img-6" src="{{url("/images/side6.svg")}}">
    <div class="container workers-container">

        <div class="swiper-container" id="workersSlider">
            <div class="swiper-wrapper">
                <!-- Slides -->
                <?php foreach ($workers as $key => $worker):?>
                <div class="swiper-slide">
                    <div class="workers-slide">
                        @include('components.items.worker', ['worker'=>$worker])
                    </div>
                </div>
                <?php endforeach;?>
            </div>
        </div>
        <div class="work-pagination-block">
            <div class="swiper-pagination work-pagination"></div>
        </div>
        <div class="choose-btn">
            <div class="choose-btn-container">
                <div class="arrow-left blog-button-prev work-button-prev"></div>
                <div class="arrow-right blog-button-next work-button-next"></div>
            </div>
        </div>
    </div>
</section>

<?php endif;?>

