<?php
/**
 * Created by PhpStorm.
 * User: lolbu
 * Date: 15.11.2018
 * Time: 1:01
 */

$trusts = \App\Models\TrustModel::where('status', \App\Models\ChooseModel::STATUS_SHOW)
    ->orderBy('position', 'asc')->get();
?>

<section id="trust">
    <div class="label-main padding-section" align="center">
        <h2>{{__('main.trust.trustLabel')}}</h2>
    </div>
    @desktop
    <div class="container trust-container">
        <?php foreach ($trusts as $trust):?>
        @include('components.items.trust_company', ['partner'=>$trust])
        <?php endforeach;?>
    </div>
    @elsedesktop
    <div class="container swiper-container" id="trustSlider" align="center">
        <div class="swiper-wrapper">
            <?php foreach ($trusts as $trust):?>
                <div class="swiper-slide">
                    @include('components.items.trust_company', ['partner'=>$trust])
                </div>
            <?php endforeach;?>
        </div>
        <div class="swiper-pagination choose-pagination trust-pagination"></div>
    </div>
    @section('scripts')
        @parent
        <script type="text/javascript">
            new Swiper('#trustSlider', {
                loop: false,
                pagination: {
                    el: '.trust-pagination',
                    clickable: true,
                },
                breakpoints: {
                    575: {
                        slidesPerView: 1,
                        spaceBetween: 20
                    },
                    // when window width is <= 640px
                    768: {
                        slidesPerView: 2,
                        spaceBetween: 30
                    } ,
                    1024: {
                        slidesPerView: 3,
                        spaceBetween: 30
                    }
                }
            });
        </script>
    @endsection
    @enddesktop
</section>

