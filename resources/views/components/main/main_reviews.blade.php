<?php
/**
 * Created by PhpStorm.
 * User: Minor946
 * Date: 11/12/2018
 * Time: 3:59 PM
 */
?>
<section id="reviews">
    <div class="label-main padding-section" align="center">
        <h2>{{__('main.reviews.reviewsLabel')}}</h2>
    </div>
    <img class="side-img side-img-7" src="{{url("/images/side7.svg")}}">
    <img class="side-img side-img-8" src="{{url("/images/side8.svg")}}">
    <div class="container reviews-video-container">
        <div class="swiper-container" id="reviewSlider">
            <div class="swiper-wrapper">
                <!-- Slides -->
                <?php
                $comments = \App\Models\CommentsModel::where('status', \App\Models\ChooseModel::STATUS_SHOW)->orderBy('created_at','DESC')->take(4)->get();
                ?>
                <?php foreach ($comments as $comment):?>
                <div class="swiper-slide">
                    @include('components.items.review_comment', ['comment'=>$comment])
                </div>
                <?php endforeach;?>
            </div>
        </div>

        <div align="center">
            <div class="swiper-pagination choose-pagination review-pagination"></div>
        </div>
        <div class="choose-btn">
            <div class="choose-btn-container">
                <div class="arrow-left review-button-prev"></div>
                <div class="arrow-right review-button-next"></div>
            </div>
        </div>
    </div>
</section>
