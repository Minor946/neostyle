<?php
/**
 * Created by PhpStorm.
 * User: Minor946
 * Date: 11/12/2018
 * Time: 4:19 PM
 */
?>
<section class="discount" id="discount">
    <img src="{{url("/images/paints.png")}}">
    <div class="discount-body">
        <h3>{{__('main.discount.discountLabel')}}</h3>
        <h2>{{__('main.discount.discountDesc')}}
            <br>{{__('main.discount.rightNow')}}</h2>
        <button class="btn-discount"  data-type="4" data-toggle="modal" data-target="#discountModal">
            {{__('main.discount.query')}}
        </button>

    </div>
</section>