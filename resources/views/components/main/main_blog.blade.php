<?php
/**
 * Created by PhpStorm.
 * User: lolbu
 * Date: 15.11.2018
 * Time: 1:00
 */

$blogs = \App\Models\BlogsModel::where('status', \App\Models\ChooseModel::STATUS_SHOW)->orderBy('position', 'desc')->take(6)->get();

$countBlog = \App\Models\BlogsModel::where('status', \App\Models\ChooseModel::STATUS_SHOW)->count();
?>

<section id="blog">
    <div class="label-main padding-section" align="center">
        <h2>{{__('main.blogs.blogsLabel')}}</h2>
    </div>

    <div class="container blog-container">
        <div class="swiper-container" id="blogSlider">
            <div class="swiper-wrapper">
                <!-- Slides -->
                <?php foreach ($blogs as $blog):?>
                <div class="swiper-slide">
                    <div class="blog-slide">
                        @include('components.items.blog_item', ['blog'=>$blog])
                    </div>
                </div>
                <?php endforeach;?>
            </div>
        </div>
        <div align="center">
            <div class="swiper-pagination choose-pagination blog-pagination"></div>
        </div>
        <div class="choose-btn">
            <div class="choose-btn-container">
                <div class="arrow-left blog-button-prev"></div>
                <div class="arrow-right blog-button-next"></div>
            </div>
        </div>
        <div align="center">
            <a class="btn btn-primary btn-main bnt-project btn-all btn-blogAll"
               href="{{url('/blogs')}}">{{ __('main.blogs.showAll') }}</a>
        </div>
    </div>

</section>