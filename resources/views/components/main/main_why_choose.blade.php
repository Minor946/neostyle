<?php
/**
 * Created by PhpStorm.
 * User: lolbu
 * Date: 15.11.2018
 * Time: 1:04
 */
?>
<section id="whychooseus">
    <div class="label-main padding-section" align="center">
        <h2>{{ __('messages.whychooseus') }}</h2>
    </div>
    <img class="side-img side-img-1" src="{{url("/images/side1.svg")}}">
    <img class="side-img side-img-2" src="{{url("/images/side2.svg")}}">
    <div class="choose-container">
        <div class="swiper-container container" id="chooseSlider">
            <div class="swiper-wrapper">
                <?php
                $chooses = \App\Models\ChooseModel::where('status', \App\Models\ChooseModel::STATUS_SHOW)->get();
                $count = 0;
                ?>
                <?php foreach ($chooses as $key => $choose):?>
                <div class="swiper-slide choose-slide">
                    @include('components.items.choose', ['choose'=>$choose])
                </div>
                <?php endforeach;?>
            </div>
            <div class="swiper-pagination choose-pagination"></div>
        </div>
        <div class="choose-btn">
            <div class="choose-btn-container">
                <div class="arrow-left choose-button-prev"></div>
                <div class="arrow-right choose-button-next"></div>
            </div>
        </div>
    </div>
</section>
