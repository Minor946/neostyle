<?php
/**
 * Created by PhpStorm.
 * User: Minor946
 * Date: 11/12/2018
 * Time: 3:25 PM
 */
?>

<section id="reviews-video">
    <div class="label-main" align="center">
        <h2>{{__('main.reviewsVideo.reviewsLabel')}}</h2>
    </div>
    <img class="side-img side-img-9" src="{{url("/images/side9.svg")}}">
    <div class="container reviews-video-container">
        <div class="swiper-container" id="videoSlider">
            <div class="swiper-wrapper">
                <!-- Slides -->
                <?php
                $reviews = \App\Models\ReviewsModel::where('status', \App\Models\ChooseModel::STATUS_SHOW)->orderBy('created_at','DESC')->take(6)->get();
                ?>
                <?php foreach ($reviews as $review):?>
                <div class="swiper-slide">
                    @include('components.items.review_video', ['review'=>$review])
                </div>
                <?php endforeach;?>
            </div>
        </div>

        <div align="center">
            <div class="swiper-pagination choose-pagination video-pagination"></div>
        </div>

        <div class="choose-btn">
            <div class="choose-btn-container">
                <div class="arrow-left video-button-prev"></div>
                <div class="arrow-right video-button-next"></div>
            </div>
        </div>
    </div>
    <div class="margin-top-40 review-channel" align="center">
        <h4 class="review-video-label">{{__('main.reviewsVideo.openChannel')}}</h4>
        <a href="{{config('youtube_chanel')}}" target="_blank">
            <img src="{{url("/images/youtube-big.svg")}}" width="244" height="77">
        </a>
    </div>
</section>