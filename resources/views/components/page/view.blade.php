<?php
/**
 * Created by PhpStorm.
 * User: lolbu
 * Date: 05.12.2018
 * Time: 1:32
 */
?>

<section class="section-white">
    <div class="container padding-top-50">
        <h2><?=$model->name?></h2><br>
        <div class="portfolio-body-desc">
            @if(isset($model->description))
                <p>{!!html_entity_decode($model->description)!!}</p>
            @endif
        </div>
    </div>
</section>
