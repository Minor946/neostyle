<?php
/**
 * Created by PhpStorm.
 * User: Minor946
 * Date: 12/7/2018
 * Time: 12:51 PM
 */

use App\Models\ChooseModel;

$jobs = \App\Models\VacancyModel::where('status', ChooseModel::STATUS_SHOW)->get();
?>

<section class="section-white padding-top-20">
    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url("/")}}">{{__('title.MainPage')}}</a></li>
                <li class="breadcrumb-item active" aria-current="page">{{__('navbar.jobs')}}</li>
            </ol>
        </nav>

        <div class="label-portfolio portfolio-data">
            <h2>{{__('navbar.jobs')}}</h2>
        </div>

        <p class="jobs-description" >
            {!! __('jobs.description') !!}
            {!! __('jobs.available') !!}
        </p>
        <div class="container portfolio-container padding-top-50" align="center">
            <div class="nav nav-pills portfolio-tab" id="nav-portfolio" role="tablist">
                @foreach ($jobs as $key => $job)
                    <a class="nav-item nav-job nav-link nav-blog-category <?=($key == 0) ? 'active' : ''?>"
                       data-toggle="tab"
                       href="#job-{{$job->id}}"
                       role="tab" aria-selected="<?=($key == 0) ? 'true' : 'false'?>"
                       data-value="{{$job->id}}">{{$job->name}}</a>
                @endforeach
            </div>

            <div class="tab-content padding-top-50">
                @foreach ($jobs as $key => $job)
                    <div class="adding-top-50 tab-pane fade   <?=($key == 0) ? 'show active' : ''?>"
                         id="job-{{$job->id}}"
                         role="tabpanel" aria-labelledby="nav-home-tab">
                        <div class="jobs-items" id="jobs-items">
                            <div class="row">
                                <div class="col-12 col-md-6" align="left">
                                    <img src="{{url('/images/vacancy1.svg')}}" alt="logo">
                                    <div class="job-label">{{__('jobs.requirements')}}</div>
                                    <div class="job-content"><?=$job->requirements?></div>
                                </div>
                                <div class="col-12 col-md-6" align="left">
                                    <img src="{{url('/images/vacancy2.svg')}}" alt="logo">
                                    <div class="job-label">{{__('jobs.responsibility')}}</div>
                                    <div class="job-content"><?=$job->responsibility?></div>
                                </div>
                                <div class="col-12 col-md-6" align="left">
                                    <img src="{{url('/images/vacancy4.svg')}}" alt="logo">
                                    <div class="job-label">{{__('jobs.conditions')}}</div>
                                    <div class="job-content"><?=$job->conditions?></div>
                                </div>
                                <div class="col-12 col-md-6" align="left">
                                    <img src="{{url('/images/vacancy3.svg')}}" alt="logo">
                                    <div class="job-label">{{__('jobs.personal')}}</div>
                                    <div class="job-content"><?=$job->personal?></div>
                                </div>
                                <div class="col-12" align="left">
                                    {!! Form::open(['action' => 'MainController@callbackJobs','id'=>'jobs-form', 'files' => true]) !!}
                                        <div class="form-group">
                                            <div class="input_file">
                                                <label for="file" class="file_label">
                                                    <i class="fa fa-paperclip" aria-hidden="true"></i>
                                                    Прикрепить резюме
                                                </label>
                                                <input id="file" type="file" name="file"/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="link"
                                                   placeholder="http//ссылка вашего портфолио">
                                        </div>

                                        <div>
                                            <button type="submit" class="btn btn-primary btn-main btn-modal">Отправить
                                                резюме
                                            </button>
                                        </div>
                                        <div class="jobs-help">
                                            <span>Если Ваше резюме нас заинтересует‚ мы Вам перезвоним и сообщим время для собеседования. </span>
                                            <span>Нажимая на кнопку &quot;Отправить резюме&quot;, вы даёте своё согласие на обработку персональных данных</span>
                                        </div>
                                    {!! Form::close() !!}
                                </div>
                            </div>

                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</section>

