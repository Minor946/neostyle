<?php
/**
 * Created by PhpStorm.
 * User: Minor946
 * Date: 11/29/2018
 * Time: 2:17 PM
 */

$sliders = \App\Models\SlidersModel::where('status', \App\Models\ChooseModel::STATUS_SHOW)
    ->orderBy('position', 'asc')->get();
?>

<section class="section-white padding-top-20">
    <?php foreach ($sliders as $slider) :?>
    <div class="swiper-slide main-slide action-slider-2" id="action-<?=$slider->id?>">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-7 action-slider-desc order-2 order-lg-1">
                    <h3><?=$slider->title_h3?></h3>
                    <h3><?=$slider->title_h2?></h3>
                    <hr style="background-color:<?=$slider->color?> !important;">
                    <br>
                    <p>
                        {!!html_entity_decode($slider->description)!!}
                    </p>
                    <div class="btn-discount-container">
                        <button class="btn btn-primary btn-main bnt-project btn-all btn-discount" data-toggle="modal"
                                data-target="#questionModal" data-type="discount" data-value="<?=$slider->id?>">{{ __('discount.make_query') }}</button>
                    </div>
                </div>
                <div class="col-12 col-lg-5 order-1 order-lg-2">
                    <img src="{{url("/storage/".$slider->image)}}">
                </div>
            </div>
        </div>
    </div>
    <?php endforeach;?>
</section>