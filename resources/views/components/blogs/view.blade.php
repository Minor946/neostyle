<?php
/**
 * Created by PhpStorm.
 * User: lolbu
 * Date: 25.11.2018
 * Time: 22:19
 */

/** @var \App\Models\BlogsModel $model */

?>
<section class="section-white padding-top-20">
    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url("/")}}">{{__('title.MainPage')}}</a></li>
                <li class="breadcrumb-item"><a href="{{url("/blogs")}}">{{__('navbar.blogs')}}</a></li>
                <li class="breadcrumb-item active" aria-current="page">{{$model->title}}</li>
            </ol>
        </nav>
        <div class="portfolio-body">
            <div class="row">
                <div class="col-12 col-md-7">
                    <h2 class="blog-h2">{{$model->title}}</h2>
                    <div class="blog-body-desc">
                        @if(isset($model->description))
                            <p>{!!html_entity_decode($model->description)!!}</p>
                        @endif
                    </div>
                </div>
                <div class="col-12  col-md-5 blog-slider">
                    <div class="portfolio-slider">
                        <div class="swiper-container swiper-portfolio swiper-blog" id="projectSlider">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <img src="{{url("/storage/".$model->image)}}">
                                </div>
                                <?php if(!empty($model->sliders)):?>
                                <?php foreach($model->sliders as $slider):?>
                                <div class="swiper-slide">
                                    <img src="{{url("/storage/".$slider)}}">
                                </div>
                                <?php endforeach;?>
                                <?php endif;?>
                            </div>
                            <div class="swiper-pagination choose-pagination portfolio-pagination"></div>
                            <div class="arrow-left portfolio-button-prev"></div>
                            <div class="arrow-right portfolio-button-next"></div>
                        </div>
                    </div>
                    <div align="center">
                        <div class="share">
                            <p>{{__('blogs.share')}}</p>
                            <p>
                                <a class="fb-share facebook-share-button"
                                   href="https://www.facebook.com/sharer/sharer.php?u={{url($_SERVER['REQUEST_URI']) }}">
                                    <i class="fa fa-facebook-square"></i>
                                </a>
                            <p>
                                <a data-pin-do="buttonBookmark" data-pin-round="true"
                                   href="https://www.pinterest.com/pin/create/button/">
                                    <i class="fa fa-pinterest"></i>
                                  </a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container blog-container padding-top-50">
        <div class="label-portfolio" align="center">
            <h2>{{__('blogs.more_news')}}</h2>
        </div>
        <div class="swiper-container" id="blogSlider">
            <div class="swiper-wrapper">
                <!-- Slides -->
                <?php
                $blogs = \App\Models\BlogsModel::where('status', \App\Models\ChooseModel::STATUS_SHOW)->take(6)->get();
                ?>
                <?php foreach ($blogs as $blog):?>
                <div class="swiper-slide slide-blog">
                    <div class="blog-slide">
                        @include('components.items.blog_item', ['blog'=>$blog])
                    </div>
                </div>
                <?php endforeach;?>
            </div>
        </div>
    </div>
</section>
