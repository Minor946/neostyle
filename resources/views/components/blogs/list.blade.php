<?php
/**
 * Created by PhpStorm.
 * User: lolbu
 * Date: 25.11.2018
 * Time: 22:19
 */

$categories = \App\Models\CategoriesModel::where('status', \App\Models\ChooseModel::STATUS_SHOW)
    ->where('type', 1)->get();
$blogs = \App\Models\BlogsModel::where('status', \App\Models\ChooseModel::STATUS_SHOW)->orderBy('position', 'desc')->get();
?>
<section class="section-white padding-top-20">

    <div class="container blog-container padding-top-50">
        <!-- Slides -->
        <div class="label-portfolio" align="center">
            <h2>{{__('navbar.blogs')}}</h2>
        </div>

        <div class="container portfolio-container" align="center">
            <div class="nav nav-pills portfolio-tab" id="nav-portfolio" role="tablist">
                <a class="nav-item nav-link active nav-blog-category" id="nav-home-tab" data-toggle="tab"
                   href="javascript:void(0);"
                   role="tab"
                   aria-controls="nav-home" data-value="*" aria-selected="true">{{__('main.portfolio.showAll')}}</a>
                @foreach ($categories as $category)
                    <a class="nav-item nav-link nav-blog-category" data-toggle="tab" href="javascript:void(0);"
                       role="tab" aria-selected="false"
                       data-value="{{$category->id}}">{{$category->name}}</a>
                @endforeach

            </div>
            <hr class="hr-portfolio">
            <div class="tab-content" align="left">
                <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                    <div id="blog-items">
                        <?php foreach ($blogs as $blog):?>
                            @include('components.items.blog_item', ['blog'=>$blog])
                        <?php endforeach;?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@section('scripts')
    @parent
    <script>
        gridBlog();
    </script>
@endsection

