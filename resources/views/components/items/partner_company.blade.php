<?php
/**
 * Created by PhpStorm.
 * User: lolbu
 * Date: 15.11.2018
 * Time: 1:58
 */

/** @var \App\Models\PartnersModel $partner */

$image = $partner->image2;
if (empty($image)) {
    $image = $partner->image;
}
?>
<div class="partner-item" align="center">
    <img class="lazy" src="{{url("/storage/".$image)}}" data-content="{{url("/storage/".$partner->image)}}" width="155">
    <a href="javascript:void(0);" class="btn btn-main btn-partners" data-toggle="modal" data-slide="{{$count}}"
       data-target="#layoutsModal">{{ __('btn.more') }}</a>
</div>
