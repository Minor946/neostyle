<?php
/**
 * Created by PhpStorm.
 * User: Minor946
 * Date: 11/12/2018
 * Time: 4:00 PM
 */


/** @var \App\Models\CommentsModel $comment */

$name = explode('.', $comment->image);
$small = $name[0]. '_small.jpg';
?>

<div class="review-comment">
    <div class="review-comment-body">
        <div class="review-comment-header" style="border-color: <?=$comment->color?> !important;">
            <div class="row">
                <div class="col-4 col-md-3">
                    <?php if(Storage::disk('public')->exists($small)) :?>
                    <img class="lazy" src="{{url("/storage/" . $small)}}" width="100" height="100">
                    <?php else:?>
                    <img class="lazy" src="{{url("/storage/".$comment->image)}}" width="100" height="100">
                    <?php endif; ?>
                </div>
                <div class="col-8 col-md-9">
                    <h4><?=$comment->username?></h4>
                    <p>
                        {!!html_entity_decode( $comment->placeholder)!!}
                    </p>
                </div>
            </div>
        </div>
        <div class="review-comment-desc">
            <p>
			 {!!html_entity_decode( $comment->comment)!!}
            </p>
        </div>
    </div>
</div>
