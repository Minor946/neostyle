<?php
/**
 * Created by PhpStorm.
 * User: Minor946
 * Date: 11/12/2018
 * Time: 3:47 PM
 */


/** @var \App\Models\ReviewsModel $review */


$name = explode('.', $review->poster);
$small = $name[0] . '_small.jpg';
?>
<a class="review-video-link" href="javascript:void(0);"
   data-video-id="<?=substr($review->url, strpos($review->url, "embed/") + 6); ?>">
    <div class="review-video">
        <div class="review-poster">
            <?php if(Storage::disk('public')->exists($small)) :?>
            <img class="lazy img-poster" src="{{url("/storage/" . $small)}}">
            <?php else:?>
            <img class="lazy img-poster" src="{{url("/storage/".$review->poster)}}">
            <?php endif; ?>
            <img class="btn-play" src="{{url("/images/youtube-play.svg")}}">
        </div>
        <div class="review-video-body">
            <h3><?=$review->name?></h3>
            <p><?=$review->short_desc?></p>
        </div>
    </div>
</a>
