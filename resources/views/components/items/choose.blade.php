<?php
/**
 * Created by PhpStorm.
 * User: lolbu
 * Date: 15.11.2018
 * Time: 2:51
 */

/** @var \App\Models\ChooseModel $choose */
?>

<?php if(isset($choose)):?>
<div class="choose-item">
    <img class="lazy"  src="{{url("/storage/".$choose->image)}}">
    <h2><?=$choose->title?></h2>
    <p>{!!html_entity_decode( $choose->description)!!}</p>
</div>
<?php endif; ?>
