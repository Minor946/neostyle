<?php
/**
 * Created by PhpStorm.
 * User: Minor946
 * Date: 11/15/2018
 * Time: 2:44 PM
 */

use Carbon\Carbon;use Intervention\Image\ImageManager;


/** @var \App\Models\PortfolioModel $project */

Carbon::setLocale(app()->getLocale());
$dt = Carbon::parse($project->build_at);
?>
<?php
$filterCategory = "";
foreach ($project->categories as $category) {
    $filterCategory .= " portfolio-category-" . $category;
}
if (empty($countRender)) {
    $countRender = 0;
}
$name = explode('.', $project->image);
$small = $name[0]. '_small.jpg';

?>

<div class="portfolio-item portfolio-style-<?=$project->categoryStyle->id?> <?=$filterCategory?> <?=($countRender > 5) ? 'hidden' : ''?>">

    <?php if(Storage::disk('public')->exists($small)) :?>
        <img class="lazy" src="{{url("/storage/" . $small)}}">
    <?php else:?>
        <img class="lazy" src="{{url("/storage/" . $project->image)}}">
    <?php endif; ?>

    <div class="portfolio-item-body">
        <h3><?=$project->name?></h3>
        <p class="portfolio-item-desc">
            <strong>{{__('main.portfolio.card.style')}}</strong> <?=$project->categoryStyle->name?><br>
            <strong>{{__('main.portfolio.card.square')}}</strong> <?=$project->square?><br>
            <strong>{{__('main.portfolio.card.location')}}</strong> <?=$project->location?><br>
            <strong>{{__('main.portfolio.card.date')}}  </strong> {{ $dt->format('Y') }}
        </p>
        <div align="center">
            <a class="btn btn-primary btn-main bnt-project" href="{{url('/'.$project->alias)}}">{{ __('btn.more') }}</a>
        </div>
    </div>
</div>