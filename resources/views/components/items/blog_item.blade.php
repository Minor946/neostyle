<?php
/**
 * Created by PhpStorm.
 * User: lolbu
 * Date: 19.11.2018
 * Time: 2:22
 */

use Carbon\Carbon;use Illuminate\Support\Facades\Storage;use Intervention\Image\ImageManager;

/** @var \App\Models\BlogsModel $blog */

Carbon::setLocale(app()->getLocale());
$dt = Carbon::parse($blog->created_at);
$isVideo = false;
if (!empty($blog->item)) {
    if (strpos($blog->item, 'youtu.be') !== false) {
        $isVideo = true;
    }
}

$name = explode('.', $blog->image);
$small = $name[0]. '_small.jpg';

?>
<div class="blog-container blog-category-<?=$blog->type?>">
<?php if($isVideo):?>
    <a class="blog-link blog-link-video " href="javascript:void(0);"
        data-video-id="<?=substr($blog->item, strpos($blog->item, "embed/") + 6); ?>">
<?php elseif(!empty($blog->item)):?>
    <a class="blog-link" href="{{url($blog->item)}}">
<?php else:?>
    <a class="blog-link" href="{{url("/blogs/".$blog->alias)}}">
<?php endif;?>
    <div class="blog-video">
        <p class="blog-time">{{ $dt->format('d.m.y') }}</p>
        <div class="blog-poster">

            <?php if(Storage::disk('public')->exists($small)) :?>
                <img class="blog-img" src="{{url("/storage/" . $small)}}">
            <?php else:?>
                <img class="blog-img" src="{{url("/storage/" . $blog->image)}}">
            <?php endif; ?>

            <?php if($isVideo):?>
            <img class="btn-play" src="{{url("/images/youtube-play.svg")}}">
            <?php endif;?>
        </div>
        <div class="blog-body">
            <p><?=$blog->title?></p>
        </div>
        <div align="left">
            <hr style="background-color:<?=$blog->color?> !important;">
        </div>
    </div>
</a>
</div>