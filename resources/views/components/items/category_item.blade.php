<?php
/**
 * Created by PhpStorm.
 * User: lolbu
 * Date: 22.11.2018
 * Time: 1:38
 */

/** @var \App\Models\CategoriesModel $category */
?>

<?php
if(empty($category)):?>
<a href="javascript:void(0);" class="btn btn-sub-category" data-parent="<?=$parent_id?>"
   data-value="*">{{__('main.portfolio.showAll')}}</a>
<?php else:?>
<a href="javascript:void(0);" class="btn btn-sub-category <?= (!empty($sub_category) && $sub_category == $category->id) ? 'active' : '' ?>" data-parent="<?=$parent_id?>"
   data-value="<?=$category->id?>"><?=$category->name?></a>
<?php endif?>
