<?php
/**
 * Created by PhpStorm.
 * User: lolbu
 * Date: 15.11.2018
 * Time: 1:47
 */

use Illuminate\Support\Facades\Storage;

/** @var \App\Models\TrustModel $partner */

$name = explode('.', $partner->image_truest);
$small = $name[0]. '_small.jpg';

$nameLogo = explode('.', $partner->image);
$smallLogo = $nameLogo[0]. '_small.jpg';
?>
<div class="trust-item">
    <?php if(Storage::disk('public')->exists($small)) :?>
        <img class="lazy" src="{{url("storage/" . $small)}}">
    <?php else:?>
        <img class="lazy" src="{{url("storage/" . $partner->image_truest)}}">
    <?php endif; ?>

    <div class="company-label">
        <a class="btn-trust" href="{{url('/'.$partner->portfolio->alias)}}">
            <?php if(Storage::disk('public')->exists($smallLogo)) :?>
                <img class="lazy" src="{{url("/storage/" . $smallLogo)}}">
            <?php else:?>
                <img class="lazy" src="{{url("/storage/" . $partner->image)}}">
            <?php endif; ?>

            <p>{{__('main.trust.viewProject')}}</p>
        </a>
    </div>
</div>
