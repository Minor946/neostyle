<?php
/**
 * Created by PhpStorm.
 * User: Minor946
 * Date: 12/13/2018
 * Time: 3:26 PM
 */

/** @var \App\Models\WorkersModel $worker */
use Illuminate\Support\Facades\Storage;


$name = explode('.', $worker->image);
$small = $name[0]. '_small.jpg';
?>


<div class="worker-item" align="center">
    <?php if(Storage::disk('public')->exists($small)) :?>
        <img class="lazy" src="{{url("/storage/" . $small)}}">
        <?php else:?>
        <img class="lazy" src="{{url("/storage/" . $worker->image)}}">
    <?php endif; ?>
    <h2>{{$worker->name}}</h2>
    <p>{{$worker->specific}}</p>
    <hr style="background-color:<?=$worker->color?> !important;">
</div>