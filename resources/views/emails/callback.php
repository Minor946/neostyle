<?php
/**
 * Created by PhpStorm.
 * User: Minor946
 * Date: 30.01.2019
 * Time: 18:28
 */

switch ($callback->type) {
    case 'main-question-form':
        $header = 'Остались вопросы (Главная страница)';
        break;
    case 'subscribe-form':
        $header = 'Подписать на новости';
        break;
    case 'first-form':
        $header = 'Бесплатная консультация';
        break;
    case 'checkList-form':
        $header = 'Скачать чеклист';
        break;
    case 'form-action':
        $header = 'Узнай стоимость ремонта';
        break;
    case 'form-price':
        $header = 'Стоимость услуг';
        break;
    case 'form-query':
        $header = 'Оставить запрос';
        break;
    case 'form-review':
        $header = 'Отзыв';
        break;
    case 'form-discount':
        $header = 'Участвовать в акции';
        break;
    default:
        $header = '';
        break;
}
?>
<h3><?= $header ?></h3>
<p><strong>Имя:</strong><?= $callback->name ?></p>
<p><strong>Телефон:</strong><?= $callback->phone ?></p>
<p><strong>Почта:</strong><?= $callback->email ?></p>
<p><strong>Сообщение:</strong><?= $callback->message ?></p>
<p><strong>Доп. информация:</strong><?= $callback->value ?></p>
<p><strong>Источник:</strong> <?= $callback->url_source ?></p>
<p><strong>Создано:</strong><?= $callback->created_at ?></p>