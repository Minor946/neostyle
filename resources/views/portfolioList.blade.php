<?php
/**
 * Created by PhpStorm.
 * User: Minor946
 * Date: 11/27/2018
 * Time: 2:21 PM
 */
?>
@extends('layouts.layout')

@section('title', __('title.Portfolio'))

@section('meta')
    @parent
    <meta name="description" lang="ru" content="{{__('meta.PortfolioDescription')}}">
    <meta name="keywords" lang="ru" content="{{__('meta.PortfolioKeywords')}}">
@endsection


@section('content')
    @include('components.portfolio.list')
@endsection
