<?php
/**
 * Created by PhpStorm.
 * User: Minor946
 * Date: 11/22/2018
 * Time: 12:54 PM
 */
?>

<div class="modal fade modal-style modal-small" id="checkListModal" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <div class="modal-label" align="center">
                <p>ЧТОБЫ СКАЧАТЬ ЧЕК - ЛИСТ
                    ЗАПОЛНИТЕ ПОЖАЛУЙСТА
                    ФОРМУ</p>
            </div>
            <div class="modal-body" id="modal-body">
                <a href="{{url('/download')}}" id="download" style="display: none"></a>
                {!! Form::open(['action' => 'MainController@callback','id'=>'checkList-form','class'=>'checklist-form']) !!}
                <div class="form-group">
                    <input name="name" type="text" class="form-control" aria-describedby="emailHelp" required
                           placeholder="Имя">
                </div>
                <div class="form-group">
                    <input name="phone" type="text" required class="form-control" placeholder="Телефон">
                </div>
                <div class="form-group">
                    <input name="email" type="email" required class="form-control" placeholder="Почта">
                </div>
                {!! Form::hidden('type', 'checkList-form') !!}
                 {!! Form::hidden('urlSource', 'url', ['class'=>'url-source']) !!}
                <div align="center">
                    <button type="submit" class="btn btn-primary btn-main btn-modal">скачать</button>
                </div>
                <div class="modal-help" align="center">
                    <span>Нажимая на кнопку &quot;скачать&quot;, вы даёте своё согласие на обработку персональных данных</span>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
