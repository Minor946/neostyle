<?php
/**
 * Created by PhpStorm.
 * User: Minor946
 * Date: 11/22/2018
 * Time: 12:54 PM
 */
?>

<div class="modal fade modal-style" id="questionModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <div class="modal-style-body" id="modal-style-body">
                <div class="modal-label" align="center">
                    <p id="label-query">СДЕЛАТЬ ЗАПРОС</p>
                    <p id="label-review">ОСТАВИТЬ ОТЗЫВ</p>
                    <p id="label-discount">УЧАСТВОВАТЬ В АКЦИИ</p>
                </div>
                <div class="modal-body">
                    <p id="text-review">
                        Здесь вы можете оставить свой отзыв о сотрудничестве с нашей компанией, если у вас есть желание
                        снять видео отзыв, вы можете сообщить об этом, и наш маркетолог свяжется с вами.
                    </p>
                    <p id="text-query">
                        Вы можете оформить заявку на разработку дизайна интерьера или ремонта, а также задать любой
                        интересующий вопрос через данную форму. Мы ответим на него максимально быстро ;)
                    </p>
                    {!! Form::open(['action' => 'MainController@callback', 'id'=>'question-form','class'=>'question-form']) !!}
                        <div class="row">
                            <div class="col-12 col-md-6">
                                <div class="form-group">
                                    <input name="name" type="text" required class="form-control" placeholder="Имя">
                                </div>
                                <div class="form-group">
                                    <input name="phone" type="text" required class="form-control" placeholder="Телефон">
                                </div>
                                <div class="form-group">
                                    <input name="email" type="email" required class="form-control" placeholder="Почта">
                                </div>
                            </div>
                            <div class="col-12 col-md-6">
                                <textarea name="message" rows="6" type="text" class="form-control"
                                          placeholder="Задать вопрос"></textarea>
                            </div>
                            {!! Form::hidden('type', '', ['id'=>'type-question']) !!}
                            {!! Form::hidden('urlSource', 'url', ['class'=>'url-source']) !!}
                            {!! Form::hidden('value', '', ['id'=>'value-question']) !!}
                        </div>
                        <div align="center">
                            <button type="submit" class="btn btn-primary btn-main btn-modal">отправить</button>
                        </div>
                        <div class="modal-help" align="center">
                            <span>Нажимая на кнопку &quot;отправить&quot;, вы даёте своё согласие на обработку персональных данных</span>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
