<?php
/**
 * Created by PhpStorm.
 * User: Minor946
 * Date: 11/22/2018
 * Time: 12:54 PM
 */
?>

<div class="modal fade modal-style modal-small" id="discountModal" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <div class="modal-label" align="center">
                <p>Для расчета стоимости ответьте <br> на 3 вопроса</p>
            </div>
            <div class="requestwizard">
                <div class="requestwizard-row setup-panel">
                    <div class="requestwizard-step">
                        <a href="#step-1" type="button" class="btn btn-primary btn-circle">1</a>
                        <p>Contact information</p>
                    </div>
                    <div class="requestwizard-step">
                        <a href="#step-2" type="button" class="btn btn-default btn-circle" disabled="disabled">2</a>
                        <p>Project information</p>
                    </div>
                    <div class="requestwizard-step">
                        <a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled">2</a>
                        <p>Stone information</p>
                    </div>
                </div>
            </div>
            {!! Form::open(['action' => 'MainController@callback','id'=>'form-price']) !!}
            <div class="modal-body" id="modal-body" align="center">
                <div class="row setup-content" id="step-1">
                    <div class="col-12">
                        <div class="col-12" align="center">
                            <img src="{{url("/images/modal-img1.svg")}}">

                            <div class="form-group">
                                <input name="valueSquare" type="text" class="form-control"
                                       aria-describedby="emailHelp"
                                       placeholder="площадь помещения, м2">
                            </div>
                            <div align="center">
                                <button class="btn btn-primary btn-main btn-modal nextBtn" type="button">Далее
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row setup-content" id="step-2">
                    <div class="col-12">
                        <div class="col-12" align="center">
                            <img src="{{url("/images/modal-img2.svg")}}">
                            <?php $type = \App\Models\ModalCalcModel::all();?>
                            <?php foreach ($type as $item):?>
                            <div class="form-group">
                                <div class="select" id="step-type-<?=$item->type?>">
                                    <input type="radio" value="" name="valueType">
                                    <span class="placeholder">Выберите...</span>
                                    <?php foreach ($item->getValuesAttribute($item->tags) as $key => $tag):?>
                                    <label class="option">
                                        <input type="radio" name="valueType" id="select-<?=$key?>" value="<?=$tag?>">
                                        <span class="title" data-value="<?=$key?>"><?=$tag?></span>
                                    </label>
                                    <?php endforeach;?>
                                </div>
                            </div>
                            <?php endforeach;?>
                            <div align="center">
                                <button class="btn btn-primary btn-main btn-modal nextBtn" type="button">Далее
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row setup-content" id="step-3">
                    <div class="col-12">
                        <div class="col-12" align="center">
                            <img src="{{url("/images/modal-img3.svg")}}">
                            <div class="form-group">
                                <input name="name" type="text" required class="form-control" placeholder="Имя">
                            </div>
                            <div class="form-group">
                                <input name="phone" type="text" required class="form-control" placeholder="Телефон">
                            </div>
                            <div class="form-group">
                                <input name="email" type="email" class="form-control" placeholder="Почта">
                            </div>
                            {!! Form::hidden('type', '',['id'=>'type-discount']) !!}
                            {!! Form::hidden('value', '',['id'=>'value-discount']) !!}
                             {!! Form::hidden('urlSource', 'url', ['class'=>'url-source']) !!}
                            <div align="center">
                                <button type="submit" class="btn btn-primary btn-main btn-modal nextBtn">Отправить
                                </button>
                            </div>
                            <div class="modal-help" align="center">
                                <span>Нажимая на кнопку &quot;отправить&quot;, вы даёте своё согласие на обработку персональных данных</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}

        </div>
    </div>
</div>
