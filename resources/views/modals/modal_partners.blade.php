<?php
/**
 * Created by PhpStorm.
 * User: Minor946
 * Date: 11/22/2018
 * Time: 2:49 PM
 */

$partnersModal = \App\Models\PartnersModel::where('status', \App\Models\ChooseModel::STATUS_SHOW)->orderBy('position', 'asc')->get();
?>

<div class="modal-body modal-partners">
    <div class="swiper-container " id="partnersModalSlider">
        <div class="swiper-wrapper">
            <?php foreach ($partnersModal as $key =>  $partnerModal):?>
                <div class="container swiper-slide">
                    <div class="modal-partners-img" align="center">
                        <img src="{{url("/storage/".$partnerModal->image)}}" width="155">
                    </div>
                    <div class="row partner-data">
                        <div class="col-12 col-md-6" align="left">
                            <div>
                                <?=$partnerModal->description?>
                            </div>
                        </div>
                        <div class="col-12 col-md-6" align="left">
                            <div class="modal-address">
                                {!! $partnerModal->address !!}
                            </div>
                            <br>
                            <?php if (isset($partnerModal->site)): ?>
                                <i class="fa fa-external-link-square"></i>
                                <?= $partnerModal->site ?>
                            <br>
                            <?php endif;?>

                            <?php if(isset($partnerModal->facebook)):?>
                                <i class="fa fa-facebook-official"></i>
                                <?=$partnerModal->facebook?>
                            <br>
                            <?php endif;?>

                            <?php if(isset($partnerModal->instagram)):?>
                                <i class="fa fa-instagram"></i>
                                <?=$partnerModal->instagram?>
                            <?php endif;?>
                        </div>
                    </div>
                </div>
            <?php endforeach;?>
        </div>
        <div align="center">
            <div class="swiper-pagination choose-pagination partners-pagination"></div>
        </div>
    </div>
    <div class="choose-btn">
        <div class="choose-btn-container">
            <div class="arrow-left partners-button-prev"></div>
            <div class="arrow-right partners-button-next"></div>
        </div>
    </div>
</div>