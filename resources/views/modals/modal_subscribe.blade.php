<?php
/**
 * Created by PhpStorm.
 * User: Minor946
 * Date: 11/22/2018
 * Time: 12:54 PM
 */
?>

<div class="modal fade modal-style modal-small" id="subscribe" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <div class="modal-style-body" id="modal-style-body">
                <div class="modal-label" align="center">
                    <p>Подписатся на новости</p>
                </div>
                <div class="modal-body" align="center">

                    {!! Form::open(['action' => 'MainController@callback','id'=>'subscribe-form', 'class'=>'subscribe-form']) !!}
                        <div class="form-group">
                            <input type="email" required class="form-control" placeholder="Почта">
                        </div>
                    {!! Form::hidden('type', 'subscribe-form') !!}
                     {!! Form::hidden('urlSource', 'url', ['class'=>'url-source']) !!}
                        <div align="center">
                            <button type="submit" class="btn btn-primary btn-main btn-modal">подписаться</button>
                        </div>
                        <div class="modal-help" align="center">
                            <span>Нажимая на кнопку &quot;отправить&quot;, вы даёте своё согласие на обработку персональных данных</span>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
