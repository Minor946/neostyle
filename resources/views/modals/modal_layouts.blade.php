<?php
/**
 * Created by PhpStorm.
 * User: Minor946
 * Date: 11/22/2018
 * Time: 12:54 PM
 */
?>

<div class="modal fade modal-style" id="layoutsModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <div class="modal-style-body" id="modal-style-body">
                @include('modals.modal_partners')
            </div>
        </div>
    </div>
</div>
