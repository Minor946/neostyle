<?php
/**
 * Created by PhpStorm.
 * User: Minor946
 * Date: 12/12/2018
 * Time: 4:32 PM
 */
?>

<div class="modal fade modal-style" id="firstModal" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>

            <div class="modal-body modal-free" id="modal-body">
                <div class="row">
                    <div class="col-12 col-md-6">
                        <div class="free-consultation-block" align="left">
                            <i class="fa fa-comment"></i>
                            <h3>ЗАПИШИТЕСЬ НА</h3>
                            <h2>БЕСПЛАТНУЮ</h2>
                            <h4>КОНСУЛЬТАЦИЮ К ДИЗАЙНЕРУ ПРЯМО СЕЙЧАС!</h4>
                        </div>
                        <img class="side-img slider-modal" src="{{url("/images/side5.svg")}}">
                        <div class="modal-help" align="center" style="padding: 15px 25px">
                            <span>Нажимая на кнопку &quot;записаться&quot;, вы даёте своё согласие на обработку персональных данных</span>
                        </div>
                    </div>
                    <div class="col-12 col-md-6">
                        {!! Form::open(['action' => 'MainController@callback', 'id'=>'first-form', 'class'=>'first-form']) !!}
                            <div class="form-group">
                                <input name="name" type="text" class="form-control" required placeholder="Имя">
                            </div>
                            <div class="form-group">
                                <input name="phone" type="text" class="form-control" required placeholder="Телефон">
                            </div>
                            <div class="form-group">
                                <input name="email" type="email" class="form-control" required placeholder="Почта">
                            </div>
                            <div align="center">
                                <button type="submit" class="btn btn-primary btn-main btn-modal">записаться</button>
                            </div>
                        {!! Form::hidden('type', 'first-form') !!}
                        {!! Form::hidden('urlSource', 'url', ['class'=>'url-source']) !!}
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
