<?php
/**
 * Created by PhpStorm.
 * User: Minor946
 * Date: 11/12/2018
 * Time: 12:35 PM
 */

/** @var \App\Models\BlogsModel $model */


?>
@extends('layouts.layout')

@section('title', $model->title)



@section('meta')
    @parent
    <meta name="description" lang="ru" content="{{__('meta.BlogPageDescription')}}">
    <meta name="keywords" lang="ru" content="{{__('meta.BlogPageKeywords')}}">

    <meta property="og:url" content="{{url($_SERVER['REQUEST_URI']) }}"/>
    <meta property="og:type" content="website"/>
    <meta property="og:title" content="{{$model->title}}"/>
    <meta property="og:description" content="{{$model->short_desc}}"/>
    <meta property="og:image" content="{{url("/storage/" . $model->image)}}"/>
@endsection


@section('content')
    @include('components.blogs.view',['model'=>$model])
@endsection
