<?php
/**
 * Created by PhpStorm.
 * User: Minor946
 * Date: 11/12/2018
 * Time: 12:35 PM
 */
?>
@extends('layouts.layout')

@section('title', __('title.MainPage'))

@section('meta')
    @parent
    <meta name="description" lang="ru" content="{{__('meta.MainPageDescription')}}">
    <meta name="keywords" lang="ru" content="{{__('meta.MainPageKeywords')}}">
@endsection


@section('content')
    @include('components.main.main_slider')
    @include('components.main.main_why_choose')
    @include('components.main.main_portfolio')
    @include('components.main.main_services')
    @include('components.main.main_checklist')
    @include('components.main.main_price')
    @include('components.main.main_workers')
    @include('components.main.main_trust')
    @include('components.main.main_discount')
    @include('components.main.main_reviews')
    @include('components.main.main_reviews_video')
    @include('components.main.main_blog')
    @include('components.main.main_question')
@endsection

@section('scripts')
    @parent
    <?php $firstLoad = session('firstLoad', 'true'); ?>
    @if($firstLoad)
        {{Session::put('firstLoad', false)}}
        <script type="text/javascript">
            function isABootstrapModalOpen() {
                return $('.modal.show').length > 0;
            }

            setTimeout(function () {
                if (isABootstrapModalOpen()) {
                    $('#firstModal').modal('show');
                }
            }, 20000);

            $("html").bind("mouseleave", function () {
                $('#firstModal').modal('show');
                $("html").unbind("mouseleave");
            });
        </script>
    @endif

@endsection