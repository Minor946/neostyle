<?php
/**
 * Created by PhpStorm.
 * User: Minor946
 * Date: 11/12/2018
 * Time: 12:35 PM
 */
?>
@extends('layouts.layout')

@section('title', __('title.Contacts'))

@section('content')
    @include('components.contacts.view')
@endsection

