<?php
/**
 * Created by PhpStorm.
 * User: lolbu
 * Date: 20.12.2018
 * Time: 3:00
 */
?>

        <!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>NeoStyle - {{__('title.ThanksPage')}}</title>
    <meta name="description" lang="ru" content="{{__('meta.ThanksPageDescription')}}">
    <meta name="description" lang="ru" content="{{__('meta.ThanksPageKeywords')}}">

    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link rel="icon" type="image/png" href="{{{ url('/images/fabicon.png') }}}">

    <link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/responsive.css') }}" rel="stylesheet" type="text/css">

        <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-34909143-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-34909143-1');
</script>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-NKVPLZ');</script>
<!-- End Google Tag Manager -->

<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
   (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
   m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
   (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

   ym(26307888, "init", {
        id:26307888,
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true,
        webvisor:true
   });
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/26307888" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->

</head>
<body style="background: white">
    <!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NKVPLZ"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<button class="hamburger hamburger--slider is-active btn-back" type="button"
        style="position: absolute; top: 10px; right: 10px;">
              <span class="hamburger-box">
                <span class="hamburger-inner"></span>
              </span>
</button>

<div class="container thanks-container" align="center">

    <img src="{{url("/images/thanks-user.svg")}}">

    <h2 class="thanks-label">
        Спасибо за заявку
        С вами обязательно свяжется наш менеджер!
    </h2>
    <img src="{{url("/images/thanks-logo.svg")}}"><br>
    <a href="javascript:history.back()" class="btn btn-primary btn-main btn-slider margin-top-40">назад</a>
</div>

<script src="{{ asset('js/app.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/swiper.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/isotope.pkgd.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/js.cookie.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/scripts.js') }}" type="text/javascript"></script>


<script>
    $(document).ready(function ($) {
        setTimeout(function () {
            window.location = document.referrer;
        }, 2000);

        $('.btn-back').on('click', function () {
            window.history.back();
        });
    });
</script>
</body>
</html>