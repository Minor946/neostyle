<?php
/**
 * Created by PhpStorm.
 * User: Minor946
 * Date: 11/12/2018
 * Time: 12:35 PM
 */

/** @var \App\Models\PagesModel $model */


?>
@extends('layouts.layout')

@section('title',  __('title.JobsPage'))

@section('meta')
    @parent
    <meta name="description" lang="ru" content="{{__('meta.JobsPageDescription')}}">
    <meta name="keywords" lang="ru" content="{{__('meta.JobsPageKeywords')}}">
@endsection


@section('content')
    @include('components.jobs.view')
@endsection
