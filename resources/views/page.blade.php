<?php
/**
 * Created by PhpStorm.
 * User: Minor946
 * Date: 11/12/2018
 * Time: 12:35 PM
 */

/** @var \App\Models\PagesModel $model */


?>
@extends('layouts.layout')

@section('title', $model->name)

@section('content')
    @include('components.page.view',['model'=>$model])
@endsection
