<?php
/**
 * Created by PhpStorm.
 * User: Minor946
 * Date: 12/11/2018
 * Time: 2:19 PM
 */
?>

@extends('layouts.layout')

@section('title', __('title.PartnersPage'))

@section('meta')
    @parent
    <meta name="description" lang="ru" content="{{__('meta.PartnersPageDescription')}}">
    <meta name="keywords" lang="ru" content="{{__('meta.PartnersPageKeywords')}}">
@endsection


@section('content')
    @include('components.partners.view')
@endsection
