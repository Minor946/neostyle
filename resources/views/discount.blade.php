<?php
/**
 * Created by PhpStorm.
 * User: Minor946
 * Date: 11/12/2018
 * Time: 12:35 PM
 */
?>
@extends('layouts.layout')

@section('title', __('title.DiscountPage'))

@section('meta')
    @parent
    <meta name="description" lang="ru" content="{{__('meta.DiscountPageDescription')}}">
    <meta name="keywords" lang="ru" content="{{__('meta.DiscountPageKeywords')}}">
@endsection



@section('content')
    @include('components.discount.view')
@endsection
